defmodule GossipDemoWeb.Router do
  use GossipDemoWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {GossipDemoWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", GossipDemoWeb do
    pipe_through :browser

    live "/", ApiLive, :index
    live "/peers", PeerLive, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", GossipDemoWeb do
  #   pipe_through :api
  # end
end
