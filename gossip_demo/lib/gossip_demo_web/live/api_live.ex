defmodule GossipDemoWeb.ApiLive do
  use GossipDemoWeb, :live_view

  defp string_to_integer(string, default) do
    case string do
      "" -> default
      num -> String.to_integer(num)
    end
  end

  @impl true
  def mount(_params, _session, socket) do
    {:ok, pid} = GossipDemo.ApiClient.start_link(self())

    {:ok, assign(socket, client: pid, no_messages: true, subscribed_to: [], auto_validate: true),
     temporary_assigns: [messages: []]}
  end

  @impl true
  def handle_info({:notification, message_id, data_type, data}, socket) do
    if socket.assigns.auto_validate do
      GossipDemo.ApiClient.validate(socket.assigns.client, message_id, true)
    end

    {:noreply,
     assign(socket,
       messages: [
         %{id: message_id, type: data_type, data: data, valid: socket.assigns.auto_validate}
         | socket.assigns.messages
       ],
       no_messages: false
     )}
  end

  @impl true
  def handle_event("subscribe", %{"data_type" => data_type}, socket) do
    data_type = string_to_integer(data_type, 123)

    :ok = GossipDemo.ApiClient.subscribe(socket.assigns.client, data_type)

    {:noreply, assign(socket, subscribed_to: [data_type | socket.assigns.subscribed_to])}
  end

  @impl true
  def handle_event("send", %{"data_type" => data_type, "data" => data, "ttl" => ttl}, socket) do
    data_type = string_to_integer(data_type, 123)
    ttl = string_to_integer(ttl, 32)

    :ok =
      GossipDemo.ApiClient.announce(
        socket.assigns.client,
        ttl,
        data_type,
        data
      )

    {:noreply, socket}
  end

  @impl true
  def handle_event("autovalidate", %{"enabled" => enabled}, socket) do
    {:noreply, assign(socket, :auto_validate, if(enabled == "true", do: true, else: false))}
  end

  @impl true
  def handle_event(
        "validate",
        %{"message_id" => message_id, "type" => type, "data" => data},
        socket
      ) do
    id = String.to_integer(message_id)
    :ok = GossipDemo.ApiClient.validate(socket.assigns.client, id, true)

    {:noreply,
     assign(socket, messages: [%{id: id, type: String.to_integer(type), data: data, valid: true}])}
  end

  @impl true
  def render(assigns) do
    ~H"""
    <div class="container">
      <h1>Gossip Demo</h1>
      <p>
        Open this page in two browsers and see local message passing in action.
        <br>
        Start a new instance of this app and connect peers
        <a href={Routes.peer_path(@socket, :index)}>here</a>
        to see peer to peer messaging in action.
      </p>
      <p>To send a message, first subscribe to the data type you want to receive. Then you can start sending messages for the data type.</p>
      <div class="row">
        <div class="column gossip-subscribe">
          <h2>Subscribe to data_type</h2>
          <form id="subscribe-form" phx-submit="subscribe">
            <label for="data_type">Data Type (Number)</label>
            <input type="number" name="data_type" placeholder="123" phx-debounce="blur"/>
            <button type="submit">Subscribe</button>
          </form>
          <h3>Currently subscribed to</h3>
          <%= if length(@subscribed_to) == 0 do %>
            <span style="color: darkred;">Nothing :(</span>
          <% else %>
          <ul>
          <%= for data_type <- @subscribed_to do %>
            <li><%= data_type %></li>
          <% end %>
          </ul>
          <% end %>
        </div>
        <div class="column gossip-send">
          <h2>Send message</h2>
          <form id="send-form" phx-submit="send">
            <div class="row">
              <div class="column">
                <label for="data_type">Data Type (Number)</label>
                <input type="number" name="data_type" placeholder="123" phx-debounce="blur"/>
              </div>
              <div class="column">
                <label for="data_type">TTL (Number)</label>
                <input type="number" name="ttl" placeholder="32" phx-debounce="blur"/>
              </div>
            </div>
            <label for="data">Content (Text)</label>
            <input type="text" name="data" phx-debounce="blur"/>
            <button type="submit">Send</button>
          </form>
        </div>
      </div>
      <hr>
      <div class="message-wrapper">
        <div class="flex">
          <h2 class="flex-shrink">Messages</h2>
          <div class="flex-grow"></div>
          <button type="button" phx-click="autovalidate" phx-value-enabled={"#{!@auto_validate}"} phx-value-test="123">
            <%= if @auto_validate, do: "Disable", else: "Enable" %> Auto-Validation
          </button>
        </div>
        <%= if @no_messages do %><p>Messages will be shown here automatically once they arrive.</p><% end %>
        <ul id="gossip-messages" phx-update="append">
        <%= for message <- @messages do %>
          <li id={message.id} class="flex">
            <div class="message-data">
              <b><%= message.type %>:</b> <%= message.data %>
            </div>
            <div class="flex-grow"></div>
            <div class="message-actions">
              <button
                class="button button-clear mb-0"
                style="height: inherit; line-height: inherit;"
                disabled={message.valid}
                phx-click="validate"
                phx-value-type={message.type}
                phx-value-data={message.data}
                phx-value-message_id={message.id}
              >
                <%= if message.valid, do: "Valid", else: "Validate" %>
              </button>
            </div>
          </li>
        <% end %>
        </ul>
      </div>
    </div>
    """
  end
end
