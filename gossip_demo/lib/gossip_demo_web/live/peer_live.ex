defmodule GossipDemoWeb.PeerLive do
  use GossipDemoWeb, :live_view

  @impl true
  def mount(_params, _session, socket) do
    peers = VoidPhone.Gossip.PeerManager.get_peers()

    :timer.send_interval(:timer.seconds(5), :check_peers)

    {:ok, assign(socket, peers: peers, connect_message: nil)}
  end

  @impl true
  def handle_info(:check_peers, socket) do
    peers = VoidPhone.Gossip.PeerManager.get_peers()

    {:noreply, assign(socket, :peers, peers)}
  end

  @impl true
  def handle_info(:clear_message, socket) do
    {:noreply, assign(socket, :connect_message, nil)}
  end

  @impl true
  def handle_event("connect", %{"address" => address}, socket) do
    Process.send_after(self(), :clear_message, :timer.seconds(5))
    with :ok <- VoidPhone.Gossip.PeerManager.connect(address) do
      {:noreply, assign(socket, connect_message: "Successfully connected to #{address}!")}
    else
      {:error, reason} ->
        {:noreply,
         assign(socket,
           connect_message: "Failed to connect to #{address}, reason: #{inspect(reason)}"
         )}
    end
  end

  @impl true
  def render(assigns) do
    ~H"""
    <div class="container">
      <h1>Gossip Demo (PeerManager)</h1>
      <div class="row">
        <div class="column gossip-subscribe">
          <h2>Connect to new peer</h2>
          <%= if @connect_message do %><p><%= @connect_message %></p><% end %>
          <form id="subscribe-form" phx-submit="connect">
            <label for="address">Address</label>
            <input type="text" name="address" placeholder="127.0.0.1:8002" phx-debounce="blur"/>
            <button type="submit">Connect</button>
          </form>
        </div>
      </div>
      <hr>
      <div class="message-wrapper">
        <h2>Peer list</h2>
        <p>This list automatically updates every 5 seconds.</p>
        <ul id="peer-list">
        <%= for {ip, port} <- @peers do %>
          <li><span><%= :inet.ntoa(ip) %>:<%= port %></span></li>
        <% end %>
        </ul>
      </div>
    </div>
    """
  end
end
