defmodule GossipDemo.ApiClient do
  use GenServer, restart: :temporary

  @gossip_announce 500
  @gossip_notify 501
  @gossip_notification 502
  @gossip_validation 503

  @impl true
  def init(target) do
    {:ok, %{target: target, socket: nil, packetizer: nil}, {:continue, :connect}}
  end

  @impl true
  def handle_continue(:connect, state) do
    api_address = Application.fetch_env!(:void_phone, :gossip) |> Map.fetch!("api_address")
    {ip, port} = VoidPhone.IpUtils.get_ip_and_port(api_address)
    {:ok, socket} = :gen_tcp.connect(ip, port, [:binary, active: false])
    {:ok, packetizer} = VoidPhone.ApiPacketizer.start_link({socket, target: self()})
    {:noreply, %{state | socket: socket, packetizer: packetizer}}
  end

  @impl true
  def handle_call({:subscribe, data_type}, _from, state = %{socket: socket}) do
    # send notify
    :ok =
      :gen_tcp.send(
        socket,
        VoidPhone.MessageUtils.build_message(
          @gossip_notify,
          <<0::size(16), data_type::integer-size(16)>>
        )
      )

    {:reply, :ok, state}
  end

  @impl true
  def handle_call({:announce, ttl, data_type, data}, _from, state = %{socket: socket}) do
    # send announcement
    :ok =
      :gen_tcp.send(
        socket,
        VoidPhone.MessageUtils.build_message(
          @gossip_announce,
          <<ttl::integer-size(8), 0::size(8), data_type::integer-size(16), data::binary()>>
        )
      )

    {:reply, :ok, state}
  end

  @impl true
  def handle_call({:validate, message_id, valid}, _from, state = %{socket: socket}) do
    :ok =
      :gen_tcp.send(
        socket,
        VoidPhone.MessageUtils.build_message(
          @gossip_validation,
          <<message_id::integer-size(16), 0::size(15), valid::size(1)>>
        )
      )

    {:reply, :ok, state}
  end

  @impl true
  def handle_call(:reconnect, _from, state = %{socket: socket, packetizer: packetizer}) do
    true = Process.exit(packetizer, :kill)
    :ok = :gen_tcp.close(socket)
    {:reply, :ok, %{state | socket: nil}, {:continue, :connect}}
  end

  @impl true
  def handle_info({:tcp, _socket, data}, state = %{packetizer: packetizer}) do
    with :ok <- handle_message(data, state) do
      # tell the packetizer to receive the next message
      # used for back-pressure
      send(packetizer, :recv_new)
      {:noreply, state}
    end
  end

  @impl true
  def handle_info({:tcp_closed, _socket}, state) do
    {:stop, :normal, state}
  end

  # a handler for the api message
  defp handle_message(
         <<msgtype::integer-size(16), message_id::integer-size(16), data_type::integer-size(16),
           data::binary()>>,
         %{target: target}
       )
       when msgtype == @gossip_notification do
    send(target, {:notification, message_id, data_type, data})
    :ok
  end

  ## Client API ##

  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end

  def reconnect(server) do
    GenServer.call(server, :reconnect)
  end

  def subscribe(server, data_type) do
    GenServer.call(server, {:subscribe, data_type})
  end

  def announce(server, ttl, data_type, data) do
    GenServer.call(server, {:announce, ttl, data_type, data})
  end

  def validate(server, message_id, valid) do
    GenServer.call(server, {:validate, message_id, if(valid, do: 1, else: 0)})
  end
end
