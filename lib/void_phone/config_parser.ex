defmodule VoidPhone.ConfigParser do
  @moduledoc """
  This module parses a Windows INI like configuration file and stores
  the information inside the application's [config](https://hexdocs.pm/elixir/Config.html).
  """

  # this function maps the lines into key value paris
  defp map_items(%{name: name, items: items}) do
    items =
      Enum.reduce(items, %{}, fn item, acc ->
        case String.split(item, "=", parts: 2) do
          [key, value] -> Map.put(acc, String.trim(key), String.trim(value))
          _other -> acc
        end
      end)

    %{name: name, items: items}
  end

  # this function matches lines beginning with `[` and begins a new chunk if
  # it is actually a correct section name
  defp reducer_fun(<<"[", _rest::binary>> = section, acc) do
    case Regex.run(~r/^\[(.*)\]$/, section) do
      [_whole_match, section_name] ->
        # we've got a new section, emit the current accumulator and start with an empty one
        {:cont, acc, %{name: section_name, items: []}}

      nil ->
        # regex did not match. assume this to be a normal line
        {:cont, update_in(acc, [:items], fn current -> [String.trim(section) | current] end)}
    end
  end

  defp reducer_fun(line, acc) do
    # a regular line, we just pass it on
    {:cont, update_in(acc, [:items], fn current -> [String.trim(line) | current] end)}
  end

  @doc """
  Parses the given stream into a map of section => key/value items.

  ## Examples

      iex> VoidPhone.ConfigParser.parse_stream(["hostkey = 123", "[gossip]", "cache_size = 3"])
      %{:global => %{"hostkey" => "123"}, "gossip" => %{"cache_size" => "3"}}
  """
  @spec parse_stream(stream :: Enum.t()) :: map()
  def parse_stream(stream) do
    stream
    |> Stream.reject(fn
      "\n" -> true
      _ -> false
    end)
    |> Stream.map(&String.trim/1)
    |> Stream.chunk_while(%{name: :global, items: []}, &reducer_fun/2, fn
      [] -> {:cont, []}
      acc -> {:cont, acc, []}
    end)
    # we now parse the key/value lines
    |> Enum.map(&map_items/1)
    # we now have a list of name, items maps and merge these into a big map
    |> Enum.reduce(%{}, fn %{name: name, items: items}, acc ->
      Map.merge(acc, %{name => items})
    end)
  end

  @doc """
  Parses the given ini like file from the given path into a map of section => key/value items.
  """
  @spec parse(path :: Path.t()) :: map()
  def parse(path) do
    File.stream!(path)
    |> parse_stream()
  end
end
