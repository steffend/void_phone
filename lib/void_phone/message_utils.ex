defmodule VoidPhone.MessageUtils do
  @doc """
  Builds a conforming VoidPhone API message for the given type.
  Valid types must (currently) be between 500 and 700

  ## Examples

      iex> VoidPhone.MessageUtils.build_message(500, "hey what's going on")
      <<0, 23, 1, 244, 104, 101, 121, 32, 119, 104, 97, 116, 39, 115, 32, 103, 111, 105, 110, 103, 32, 111, 110>>

      iex> VoidPhone.MessageUtils.build_message(1000, "hey what's going on")
      ** (FunctionClauseError) no function clause matching in VoidPhone.MessageUtils.build_message/2
  """
  def build_message(type, msg) when byte_size(msg) <= 65535 and type >= 500 and type <= 700 do
    # we add 4 here as the length consists of payload AND header, with 2 bytes for the length
    # and two more bytes for the message type
    msg_length = byte_size(msg) + 4

    <<msg_length::integer-size(16), type::integer-size(16), msg::binary()>>
  end
end
