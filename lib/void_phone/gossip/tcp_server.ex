defmodule VoidPhone.Gossip.TcpServer do
  @moduledoc """
  This module implements a macro for building a simple TCP server
  that starts a new process for every connecting client.

  ## Usage

      defmodule MyTCPServer do
        use VoidPhone.Gossip.TcpServer,
          supervisor: MyDynamicSupervisor,
          worker: MyWorker,
          listen_opts: [:binary, packet: :line, active: :once, reuseaddr: true]
      end

  For real usage, see `VoidPhone.Gossip.Api` or `VoidPhone.Gossip.P2p`.
  """

  defmacro __using__(opts) do
    supervisor = Keyword.fetch!(opts, :supervisor)
    worker = Keyword.fetch!(opts, :worker)
    type = Keyword.get(opts, :type, "")

    listen_opts =
      Keyword.get(opts, :listen_opts, [:binary, packet: 0, active: :once, reuseaddr: true])

    quote do
      use Task
      require Logger

      @supervisor unquote(supervisor)
      @worker unquote(worker)
      @server_type unquote(type)
      @listen_opts unquote(listen_opts)

      # creates a new listening socket for the given options
      defp setup_socket(opts) do
        # the ip address is specified in the format host:port
        # host can be a hostname, or an ipv4/ipv6 address
        listen_address = Keyword.get(opts, :listen_address, "127.0.0.1:7001")

        {ip, port} = VoidPhone.IpUtils.get_ip_and_port(listen_address)

        {:ok, listen_socket} =
          :gen_tcp.listen(
            port,
            [{:ip, ip} | @listen_opts]
          )

        Logger.info(
          "Listening on #{inspect(ip)} port #{inspect(port)} for #{@server_type} connections..."
        )

        listen_socket
      end

      @doc """
      Starts the Gossip API server.
      """
      @spec start_link(opts :: keyword()) :: {:ok, pid()} | {:error, any()}
      def start_link(opts \\ []) do
        listen_socket = Keyword.get_lazy(opts, :listen_socket, fn -> setup_socket(opts) end)

        Task.start_link(__MODULE__, :accept, [listen_socket, opts])
      end

      @doc false
      def accept(listen_socket, opts \\ []) do
        loop_acceptor(listen_socket, opts)
      end

      # we recursively loop on the open socket
      # and pass accepted clients to a new worker process
      defp loop_acceptor(listen_socket, opts) do
        {:ok, socket} = :gen_tcp.accept(listen_socket)

        with {:ok, peer} <- :inet.peername(socket) do
          Logger.debug("#{@server_type}: client connected: #{inspect(peer)}...")
        end

        # for testing, we can override the supervisor
        # and the worker modules
        supervisor = Keyword.get(opts, :supervisor, @supervisor)
        worker = Keyword.get(opts, :worker, @worker)

        {:ok, pid} =
          DynamicSupervisor.start_child(
            supervisor,
            {worker, {socket, opts}}
          )

        :gen_tcp.controlling_process(socket, pid)
        loop_acceptor(listen_socket, opts)
      end
    end
  end
end
