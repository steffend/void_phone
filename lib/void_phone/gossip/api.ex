defmodule VoidPhone.Gossip.Api do
  @moduledoc """
  This module implements the TCP entrypoint for the API connections.

  For details about API connections, see `VoidPhone.Gossip.ApiWorker`.
  """

  alias VoidPhone.Gossip

  use VoidPhone.Gossip.TcpServer,
    supervisor: Gossip.ApiSupervisor,
    worker: Gossip.ApiWorker,
    type: "api",
    listen_opts: [:binary, packet: :raw, active: false, reuseaddr: true]

  defp do_broadcast(data_type, data, ttl, message_id, registry, knowledge_base) do
    case Cachex.get(knowledge_base, message_id) do
      {:ok, {_data_type, _data, _ttl, _originator}} ->
        # we already know this message!
        Logger.info("api: not spreading external data, as the message is a duplicate!")
        {:ok, message_id}

      {:ok, nil} ->
        # nothing found in the knowledge base, strange...
        Registry.dispatch(registry, data_type, fn entries ->
          # there are subscribers, save data into local knowledge base
          Cachex.put(knowledge_base, message_id, {data_type, data, ttl, self()})
          # notify internal subscribers (but not the one that actually called this function)
          for {pid, _} <- entries do
            # do not send back to the pid that actually called this function
            if pid != self(), do: send(pid, {:gossip_notification, message_id, data_type, data})
          end
        end)

        {:ok, message_id}
    end
  end

  @doc """
  Broadcasts a message locally. Creates a global message id if it does not exist
  and stores the message in the knowledge base.
  """
  @spec broadcast(
          data_type :: binary(),
          data :: binary(),
          ttl :: integer(),
          message_id :: integer() | nil,
          registry :: module(),
          knowledge_base :: atom()
        ) :: {:ok, message_id :: integer()} | {:error, any()}
  def broadcast(
        data_type,
        data,
        ttl,
        message_id,
        registry,
        knowledge_base
      ) do
    # check if there are any registered clients for this data type
    case Registry.lookup(registry, data_type) do
      [_ | _] ->
        # generate global message id if it does not exist
        message_id =
          case message_id do
            nil ->
              num = :rand.uniform(trunc(:math.pow(2, 16))) - 1
              :erlang.phash2({num, data_type, data})

            other ->
              other
          end

        do_broadcast(data_type, data, ttl, message_id, registry, knowledge_base)

      _other ->
        {:error, :not_registered}
    end
  end
end
