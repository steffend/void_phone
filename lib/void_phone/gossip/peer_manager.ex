defmodule VoidPhone.Gossip.PeerManager do
  @moduledoc """
  The `VoidPhone.Gossip.PeerManager` is responsible for connecting the peers.
  Initially, it will try to connect to the bootstrapping node. If this fails, it
  will periodically try to connect again.

  Once connected, the PeerManager exchanges information about the available peers.
  """
  use GenServer

  require Logger

  alias VoidPhone.Gossip.{PeerRegistry, BrahmsSampler, P2pUtils}
  alias VoidPhone.Gossip.{P2pSupervisor, P2pWorker, NotificationRegistry}

  @impl true
  def init(opts) do
    Logger.info("Starting Gossip PeerManager...")
    name = Keyword.get(opts, :name, __MODULE__)
    bootstrapper = Keyword.get(opts, :bootstrapper)
    registry = Keyword.get(opts, :registry, PeerRegistry)
    api_registry = Keyword.get(opts, :api_registry, NotificationRegistry)
    supervisor = Keyword.get(opts, :supervisor, P2pSupervisor)
    worker = Keyword.get(opts, :p2p_worker, P2pWorker)
    max_peers = Keyword.get(opts, :max_peers, 32)
    p2p_address = Keyword.fetch!(opts, :address)
    alpha = Keyword.get(opts, :alpha, 0.45)
    beta = Keyword.get(opts, :alpha, 0.45)
    gamma = Keyword.get(opts, :alpha, 0.1)
    push_interval = Keyword.get(opts, :push_interval, 30) |> :timer.seconds()
    pull_interval = Keyword.get(opts, :pull_interval, 30) |> :timer.seconds()
    update_interval = Keyword.get(opts, :update_interval, 30) |> :timer.seconds()
    sampler_check_interval = Keyword.get(opts, :sampler_check_interval, 60) |> :timer.seconds()
    sampler_check_dest = Keyword.get(opts, :sampler_check_dest, self())
    sampler_check_ping_timeout = Keyword.get(opts, :sampler_check_ping_timeout, 5000)

    retry_handler =
      Keyword.get(opts, :retry_handler, fn retry -> 1000 * trunc(:math.pow(2, retry)) end)

    samplers =
      Keyword.get_lazy(opts, :samplers, fn ->
        for(_i <- 1..max_peers, do: BrahmsSampler.new())
      end)

    # seed the random number generator for testing
    if seed = Keyword.get(opts, :seed) do
      :rand.seed(:default, seed)
    end

    send(self(), :bootstrap)

    :timer.send_interval(push_interval, :push_peers)
    :timer.send_interval(pull_interval, :pull_peers)
    :timer.send_interval(update_interval, :update_view)
    :timer.send_interval(sampler_check_interval, :check_sampler)

    {:ok,
     %{
       name: name,
       address: p2p_address,
       self: VoidPhone.IpUtils.get_ip_and_port(p2p_address),
       registry: registry,
       api_registry: api_registry,
       supervisor: supervisor,
       worker: worker,
       # the current active set of connected peers
       # peers are identified by their public address
       # saved in the form of a tuple: {ip, port}
       # also called "local view"
       peers: MapSet.new(),
       # push and pull sets
       push_peers: MapSet.new(),
       pull_peers: MapSet.new(),
       # mapping of pid -> {ip, port}
       pid_peer_map: Map.new(),
       # brahms parameters
       alpha: alpha,
       beta: beta,
       gamma: gamma,
       # the address of the bootstrapping node
       bootstrapper: bootstrapper,
       # the current delay when there was an error connecting
       # always doubled until the maximum value of 5 minutes is reached
       bootstrap_delay: 1000,
       # the maximum number of peers we should connect to;
       # used for the size of the local view, as well as the sampler
       # in brahms known as l1 and l2
       max_peers: max_peers,
       # the list of samplers
       samplers: samplers,
       sampler_check_dest: sampler_check_dest,
       sampler_check_ping_timeout: sampler_check_ping_timeout,
       # allow the retry handler to be changed when testing
       retry_handler: retry_handler
     }}
  end

  @impl true
  def handle_call(
        {:connect, {ip, port}},
        from,
        state = %{peers: peers, max_peers: l1}
      ) do
    cond do
      MapSet.size(peers) >= l1 ->
        {:reply, {:error, :peer_limit_reached}, state}

      true ->
        Logger.info("Peer Manager: connecting to new peer #{inspect({ip, port})}")
        pid = self()

        do_connect({ip, port}, state, fn result ->
          send(pid, {:reply, from, result})
        end)

        {:noreply, state}
    end
  end

  @impl true
  def handle_call({:register, pid, peer}, _from, state = %{max_peers: l1, peers: peers}) do
    Process.monitor(pid)

    cond do
      MapSet.size(peers) >= l1 ->
        # peer limit reached, treat the same way as a push request
        {:reply, :ok,
         state
         |> Map.update!(:push_peers, fn peers -> MapSet.put(peers, peer) end)
         |> Map.update!(:pid_peer_map, fn map -> Map.put(map, pid, peer) end)}

      true ->
        Logger.info("Registering new p2p connection from #{inspect(peer)}")
        # peer limit not reached yet, add node to local view!
        {:reply, :ok,
         state
         |> Map.update!(:peers, fn peers -> MapSet.put(peers, peer) end)
         |> Map.update!(:samplers, fn samplers -> update_samplers(samplers, peer) end)
         |> Map.update!(:pid_peer_map, fn map -> Map.put(map, pid, peer) end)}
    end
  end

  @impl true
  def handle_call(:get_peers, _from, state = %{peers: peers}) do
    {:reply, MapSet.to_list(peers), state}
  end

  @impl true
  def handle_call(:get_peer_pids, _from, state = %{peers: peers, registry: registry}) do
    {:reply, MapSet.to_list(peers) |> peers_to_pid(registry), state}
  end

  @impl true
  def handle_cast({:push_request, _pid, remote_address}, state) do
    {:noreply, Map.update!(state, :push_peers, fn peers -> MapSet.put(peers, remote_address) end)}
  end

  @impl true
  def handle_cast({:pull_response, peers}, state = %{self: self}) do
    # we do not want to connect to ourself
    peers = Enum.reject(peers, &(&1 == self))

    {:noreply,
     Map.update!(state, :pull_peers, fn existing -> MapSet.union(existing, MapSet.new(peers)) end)}
  end

  @impl true
  def handle_info(:bootstrap, %{bootstrapper: nil} = state) do
    Logger.info("PeerManager: no bootstrapper. We'll be lonely until a peer connects...")
    {:noreply, state}
  end

  @impl true
  def handle_info(
        :bootstrap,
        %{bootstrapper: host, bootstrap_delay: delay} = state
      ) do
    Logger.info("PeerManager: trying to establish p2p connection with #{inspect(host)}...")

    new_delay = min(delay * 2, :timer.minutes(5))

    {ip, port} = VoidPhone.IpUtils.get_ip_and_port(host)
    pid = self()

    do_connect({ip, port}, state, fn result ->
      case result do
        :ok ->
          send(pid, :bootstrap_done)

        {:already_connected, _pid} ->
          send(pid, :bootstrap_done)

        other ->
          Logger.info(
            "Failed to connect to bootstapper at #{inspect(host)} (#{inspect(other)}). Trying again..."
          )

          # exponential backoff, max 5 minutes
          Process.send_after(pid, :bootstrap, new_delay)
      end
    end)

    {:noreply, %{state | bootstrap_delay: new_delay}}
  end

  @impl true
  def handle_info(:bootstrap_done, state) do
    {:noreply, %{state | bootstrap_delay: 1000}}
  end

  @impl true
  def handle_info({:DOWN, _ref, :process, pid, reason}, state = %{pid_peer_map: pid_to_peer}) do
    case Map.get(pid_to_peer, pid) do
      nil ->
        Logger.error("PeerManager: got DOWN message for unknown connection (#{inspect(pid)})")
        {:stop, :abnormal, state}

      host ->
        Logger.info(
          "PeerManager: p2p connection to #{inspect(host)} with pid #{inspect(pid)} went down (#{inspect(reason)})."
        )

        # we do not remove the peer from the samplers, as the reason for the disconnect
        # could be temporary or even caused by us, disconnecting because the peer
        # is no longer in the local view; in this case we still want to maintain
        # it in the history of the samplers. If the node is really down, it will
        # be removed by the regular sampler checks.
        {:noreply,
         state
         |> Map.update!(:peers, fn peers -> MapSet.delete(peers, host) end)
         |> Map.update!(:push_peers, fn peers -> MapSet.delete(peers, host) end)
         |> Map.update!(:pid_peer_map, fn map -> Map.delete(map, pid) end),
         {:continue, :process_down}}
    end
  end

  @impl true
  def handle_info(:pull_peers, state = %{max_peers: l1, beta: beta}) do
    requests = ceil(l1 * beta)

    random_peers(state, requests)
    |> Enum.each(fn pid -> send(pid, :pull_request) end)

    {:noreply, state}
  end

  @impl true
  def handle_info(:push_peers, state = %{max_peers: l1, alpha: alpha}) do
    requests = ceil(l1 * alpha)

    random_peers(state, requests)
    |> Enum.each(fn pid -> send(pid, :push_request) end)

    {:noreply, state}
  end

  @impl true
  def handle_info(
        :update_view,
        state = %{
          peers: peers,
          alpha: alpha,
          beta: beta,
          gamma: gamma,
          max_peers: l1,
          push_peers: v_push,
          pull_peers: v_pull,
          samplers: samplers
        }
      ) do
    cond do
      MapSet.size(v_push) > 0 and MapSet.size(v_pull) > 0 ->
        # take alpha * l1 items from the push set
        new_push =
          Enum.take_random(v_push, ceil(alpha * l1))
          |> MapSet.new()

        # take beta * l1 items from the pull set
        new_pull =
          Enum.take_random(v_pull, ceil(beta * l1))
          |> MapSet.new()

        # take gamme * l1 items from the samplers
        new_sample = sample(samplers, ceil(gamma * l1)) |> MapSet.new()

        view =
          MapSet.union(new_push, new_pull)
          |> MapSet.union(new_sample)

        push_pull_ids =
          MapSet.union(MapSet.new(v_push), MapSet.new(v_pull))
          |> MapSet.to_list()

        state =
          state
          |> update_view(view)
          |> Map.update!(:samplers, fn samplers -> update_samplers(samplers, push_pull_ids) end)
          |> Map.put(:push_peers, MapSet.new())
          |> Map.put(:pull_peers, MapSet.new())

        {:noreply, state}

      true ->
        Logger.debug("""
        Skipping view update... #{inspect(peers)}
        push_peers: #{inspect(v_push)}
        pull_peers: #{inspect(v_pull)}
        """)

        state =
          state
          |> Map.put(:push_peers, MapSet.new())
          |> Map.put(:pull_peers, MapSet.new())

        {:noreply, state}
    end
  end

  @impl true
  def handle_info(
        :check_sampler,
        state = %{
          samplers: samplers,
          sampler_check_dest: pid,
          sampler_check_ping_timeout: timeout
        }
      ) do
    peers =
      Enum.map(samplers, fn sampler -> BrahmsSampler.sample(sampler) end)
      |> Enum.reject(&is_nil/1)
      |> Enum.uniq()

    # do blocking task in background
    Task.start(fn ->
      {_filtered_task_results, dead_peers} =
        Task.async_stream(peers, fn peer -> P2pUtils.ping(peer, timeout) end,
          ordered: false,
          timeout: 10_000,
          on_timeout: :kill_task
        )
        |> Enum.zip(peers)
        |> Enum.filter(fn
          # ping returns :ok when the peer is alive, we want everything else
          {{:ok, result}, _peer} -> result != :ok
          # when the task itself fails, the peer is also not assumed to be alive
          {_other, _peer} -> true
        end)
        |> Enum.unzip()

      new_samplers =
        Enum.reduce(dead_peers, samplers, fn peer, acc ->
          Logger.debug("Removing dead peer from samplers: #{inspect(peer)}")
          remove_sampler_element(acc, peer)
        end)

      send(pid, {:update_samplers, new_samplers})
    end)

    {:noreply, state}
  end

  @impl true
  def handle_info({:update_samplers, new_samplers}, state) do
    {:noreply, %{state | samplers: new_samplers}}
  end

  @impl true
  def handle_info({:try_connect, peer, retry}, state) do
    Logger.info("Connecting to #{inspect(peer)}...")
    pid = self()

    do_connect(peer, state, fn result ->
      send(pid, {:peer_connected, peer, result, retry})
    end)

    {:noreply, state}
  end

  @impl true
  def handle_info({:peer_connected, peer, result, retry}, state)
      when retry >= 3 and result != :ok do
    Logger.info(
      "PeerManager: Failed to connect to #{inspect(peer)} after #{retry} retries. Giving up."
    )

    {:noreply,
     state
     |> Map.update!(:peers, fn peers -> MapSet.delete(peers, peer) end)
     |> Map.update!(:pull_peers, fn peers -> MapSet.delete(peers, peer) end)
     |> Map.update!(:push_peers, fn peers -> MapSet.delete(peers, peer) end)
     # when we could not connect 3 times, the peer is not considered alive
     # therefore it should also not be in the sampler any more
     |> Map.update!(:samplers, fn samplers -> remove_sampler_element(samplers, peer) end)}
  end

  @impl true
  def handle_info({:peer_connected, peer, result, retry}, state = %{retry_handler: retry_handler}) do
    case result do
      :ok ->
        nil

      {:already_connected, _pid} ->
        nil

      other ->
        Logger.info("Failed to connect to #{inspect(peer)} (#{inspect(other)}). Trying again...")

        # exponential backoff
        Process.send_after(
          self(),
          {:try_connect, peer, retry + 1},
          retry_handler.(retry + 1)
        )
    end

    {:noreply, state}
  end

  @impl true
  def handle_info({:reply, from, result}, state) do
    GenServer.reply(from, result)

    {:noreply, state}
  end

  @impl true
  def handle_continue(:process_down, state = %{peers: peers}) do
    cond do
      MapSet.size(peers) == 0 ->
        # we lost all our peers, we'll try to connect to the bootstrapper again
        send(self(), :bootstrap)

      true ->
        nil
    end

    {:noreply, state}
  end

  defp update_samplers(samplers, element) do
    Enum.map(samplers, fn sampler -> BrahmsSampler.next(sampler, element) end)
  end

  defp sample(samplers, n) do
    Enum.map(samplers, fn sampler -> BrahmsSampler.sample(sampler) end)
    # when a host disconnects, there can be nil entries
    |> Enum.reject(&is_nil/1)
    |> Enum.take_random(n)
  end

  defp update_view(state = %{peers: peers, registry: registry}, view) do
    old_peers = MapSet.to_list(peers)
    new_peers = MapSet.to_list(view)

    to_disconnect = old_peers -- new_peers

    # terminate p2p connection, as these pids are now missing from our view
    # BUT: disconnected peers DO REMAIN in the sampler, as long as the period
    # checks succeed
    Enum.each(to_disconnect, fn address ->
      case Registry.lookup(registry, address) do
        [] ->
          nil

        [{pid, _}] ->
          Process.exit(pid, :kill)
      end
    end)

    # filter view and try to connect to missing peers
    view =
      Enum.filter(view, fn address ->
        case Registry.lookup(registry, address) do
          [] ->
            # try to connect to new peer
            send(self(), {:try_connect, address, 0})
            false

          [{_pid, _}] ->
            true
        end
      end)
      |> MapSet.new()

    Map.put(state, :peers, view)
  end

  # reinitializes all sampler that currently
  # hold the specified element
  defp remove_sampler_element(samplers, element) do
    Enum.map(samplers, fn sampler ->
      case BrahmsSampler.sample(sampler) do
        ^element -> BrahmsSampler.new()
        _ -> sampler
      end
    end)
  end

  # samples n random peers from the local view
  defp random_peers(%{peers: peers, registry: registry}, n) do
    Enum.take_random(peers, n)
    |> peers_to_pid(registry)
  end

  defp peers_to_pid(peer_names, registry) do
    Enum.map(peer_names, fn peer_name ->
      case Registry.lookup(registry, peer_name) do
        [] -> nil
        [{pid, _}] -> pid
      end
    end)
    |> Enum.reject(&is_nil/1)
  end

  defp do_connect_helper(
         {ip, port} = peer,
         _state = %{
           address: p2p_address,
           name: peer_manager,
           registry: registry,
           api_registry: api_registry,
           supervisor: supervisor,
           worker: worker
         }
       ) do
    with [] <- Registry.lookup(registry, peer),
         # as of Erlang/OTP 24.0.5 a bug was fixed that prevented specifying the local port to use
         # when initiating a tcp connection. now we can make sure to always use the p2p port for
         # outgoing connections too.
         {:ok, socket} <-
           :gen_tcp.connect(ip, port, [
             :binary,
             packet: 2,
             active: :once,
             reuseaddr: true
           ]),
         {:ok, pid} <-
           DynamicSupervisor.start_child(
             supervisor,
             {worker,
              {socket,
               sock_type: :initiated,
               listen_address: p2p_address,
               peer_manager: peer_manager,
               api_registry: api_registry,
               p2p_registry: registry}}
           ),
         :ok <- :gen_tcp.controlling_process(socket, pid) do
      :ok
    else
      [{pid, _}] -> {:already_connected, pid}
      {:error, reason} -> {:error, reason}
      other -> {:error, other}
    end
  end

  # connects to the specified peer
  defp do_connect(peer, state, callback) do
    Logger.debug("connecting to #{inspect(peer)}")

    Task.start(fn ->
      result = do_connect_helper(peer, state)
      callback.(result)
    end)
  end

  # Client API

  def start_link(opts \\ []) do
    name = Keyword.get(opts, :name, __MODULE__)
    GenServer.start_link(__MODULE__, opts, name: name)
  end

  @doc """
  Establishes a p2p connection with the specified peer
  (ip:port or hostname:port string like in the ini config) or {ip, port} tuple.
  """
  @spec connect(server :: GenServer.server(), peer :: String.t()) :: :ok | {:error, any()}
  def connect(server \\ __MODULE__, peer)

  def connect(server, peer) when is_binary(peer) do
    {ip, port} = VoidPhone.IpUtils.get_ip_and_port(peer)

    GenServer.call(server, {:connect, {ip, port}})
  end

  def connect(server, {ip, port}) do
    GenServer.call(server, {:connect, {ip, port}})
  end

  @doc """
  Registers the calling pid to be our connection to the specified remote_address.
  The remote_address (which is our peer_id) is added to the list of peers.
  """
  @spec register(server :: GenServer.server(), remote_address :: tuple(), registry :: module()) ::
          :ok | {:error, any()}
  def register(server \\ __MODULE__, remote_address, registry \\ PeerRegistry) do
    # we ensure that only one connection to the specified remote_address exists
    # by using a registry with unique keys
    with {:ok, _} <- Registry.register(registry, remote_address, []) do
      GenServer.call(server, {:register, self(), remote_address})
    end
  end

  @spec push(server :: GenServer.server(), remote_address :: tuple(), registry :: module()) ::
          :ok | {:error, any()}
  def push(server \\ __MODULE__, remote_address, registry \\ PeerRegistry) do
    case Registry.lookup(registry, remote_address) do
      [] ->
        # new connection
        with {:ok, _} <- Registry.register(registry, remote_address, []) do
          GenServer.call(server, {:register, self(), remote_address})
        end

      [{pid, _}] ->
        # push from already connected peer
        GenServer.cast(server, {:push_request, pid, remote_address})
    end
  end

  @spec pull_response(server :: GenServer.server(), peers :: list()) :: :ok | {:error, any()}
  def pull_response(server \\ __MODULE__, peers) do
    GenServer.cast(server, {:pull_response, peers})
  end

  @doc """
  Returns the current list of peers.
  """
  @spec get_peers(server :: GenServer.server()) :: [String.t()]
  def get_peers(server \\ __MODULE__) do
    GenServer.call(server, :get_peers)
  end

  @doc """
  Returns the current list of peer connection pids.
  """
  @spec get_peer_pids(server :: GenServer.server()) :: [pid()]
  def get_peer_pids(server \\ __MODULE__) do
    GenServer.call(server, :get_peer_pids)
  end
end
