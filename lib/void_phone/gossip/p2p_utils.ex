defmodule VoidPhone.Gossip.P2pUtils do
  @moduledoc """
  This module provides some utility functions for peer-to-peer connections.
  """

  @doc """
  Pings the specified peer. Tries to re-use an existing connection if possible.
  """
  @spec ping(peer :: tuple(), timeout :: integer()) :: :ok | {:error, reason :: any()}
  def ping(peer, timeout \\ 5000) do
    case Registry.lookup(VoidPhone.Gossip.PeerRegistry, peer) do
      [] ->
        # we run the check in a new process
        # to enforce that the socket that is created
        # is also closed when we finish
        connect_and_ping(peer, timeout)

      [{pid, _}] ->
        :pong = VoidPhone.Gossip.P2pWorker.ping(pid, timeout)
        :ok
    end
  end

  # creates a new socket, sends a ping;
  # and waits for a pong message to arrive
  defp connect_and_ping({ip, port}, timeout) do
    case :gen_tcp.connect(
           ip,
           port,
           [
             :binary,
             packet: 2,
             active: false,
             reuseaddr: true
           ],
           timeout
         ) do
      {:ok, socket} -> do_ping_pong(socket)
      _other -> :error
    end
  end

  # sends the actual ping; then waits and handles errors
  defp do_ping_pong(socket) do
    :gen_tcp.send(socket, :erlang.term_to_binary(:ping))
    wait_for(socket, :pong)
  rescue
    _fail -> :error
  after
    :gen_tcp.close(socket)
  end

  # waits until the specific message arrives on the socket
  defp wait_for(socket, message) do
    Stream.repeatedly(fn -> :gen_tcp.recv(socket, 0, 1000) end)
    |> Stream.map(fn {:ok, data} -> :erlang.binary_to_term(data) end)
    |> Enum.reduce_while(:error, fn
      ^message, _acc ->
        {:halt, :ok}

      _other, acc ->
        {:cont, acc}
    end)
  end

  # calculates the proof of work for the given data and difficulty
  # by using the attempt as nonce
  # sends a message to target when successful, otherwise recurses
  defp calc_pow(target, difficulty, data, attempt) do
    nonce = <<attempt::integer-size(64)>>

    msg = <<data::binary(), nonce::binary()>>
    hash = :crypto.hash(:sha256, msg)

    case hash do
      <<0::size(difficulty)-unit(8), _rest::binary()>> ->
        # yay, finally we are done!
        send(target, {:nonce, nonce})

      _other ->
        # try again...
        calc_pow(target, difficulty, data, attempt + 1)
    end
  end

  @doc """
  Solves the given proof of work challenge and executes the given
  callback once a nonce is found (or a timeout occurs).
  """
  @spec async_pow(
          difficulty :: non_neg_integer(),
          data :: binary(),
          timeout :: non_neg_integer(),
          callback :: fun()
        ) :: {:ok, pid()}
  def async_pow(difficulty, data, timeout, callback) do
    Task.start(fn ->
      target = self()

      tasks =
        Enum.map(1..System.schedulers_online(), fn _ ->
          # Start a hashing task on each scheduler with a unique random seed
          Task.async(fn ->
            calc_pow(
              target,
              difficulty,
              data,
              :rand.uniform(10_000_000_000)
            )
          end)
        end)

      receive do
        {:nonce, nonce} ->
          # we found a nonce, kill all other tasks
          Enum.each(tasks, fn task -> Task.shutdown(task, :brutal_kill) end)
          callback.({:ok, nonce})
      after
        timeout -> callback.({:error, :challenge_timeout})
      end
    end)
  end

  @doc """
  Solves the given proof of work challenge.

  ## Examples

      iex> {:ok, nonce} = VoidPhone.Gossip.P2pUtils.pow(1, "hello", 1000)
      iex> VoidPhone.Gossip.P2pUtils.valid_pow?(1, "hello", nonce)
      true

  """
  @spec pow(
          difficulty :: non_neg_integer(),
          data :: binary(),
          timeout :: non_neg_integer()
        ) :: {:ok, nonce :: binary()} | {:error, :challenge_timeout}
  def pow(difficulty, data, timeout) do
    # we use a task to make sure that the message does not
    # arrive before we enter the receive block
    Task.async(fn ->
      target = self()

      async_pow(difficulty, data, timeout, fn result ->
        send(target, {:pow_result, result})
      end)

      receive do
        {:pow_result, result} -> result
      end
    end)
    |> Task.await()
  end

  @doc """
  Checks if the given nonce is valid for the given challenge.

  ## Examples

      iex> VoidPhone.Gossip.P2pUtils.valid_pow?(0, "hello", <<>>)
      true

      iex> VoidPhone.Gossip.P2pUtils.valid_pow?(3, "hello_world", <<0, 0, 0, 0, 33, 118, 38, 159>>)
      true

      iex> VoidPhone.Gossip.P2pUtils.valid_pow?(3, "hello_world", <<0, 0, 0, 0, 33, 118, 38, 160>>)
      false

  """
  @spec valid_pow?(difficulty :: non_neg_integer(), data :: binary(), nonce :: binary()) ::
          boolean()
  def valid_pow?(difficulty, data, nonce) do
    hash = :crypto.hash(:sha256, <<data::binary(), nonce::binary()>>)

    case hash do
      <<0::size(difficulty)-unit(8), _rest::binary()>> ->
        true

      _other ->
        false
    end
  end
end
