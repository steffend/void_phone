defmodule VoidPhone.Gossip.ApiWorker do
  use GenServer, restart: :temporary
  require Logger

  alias VoidPhone.Gossip.{Api, P2p, NotificationRegistry, PeerManager}
  alias VoidPhone.ApiPacketizer

  @gossip_announce 500
  @gossip_notify 501
  @gossip_notification 502
  @gossip_validation 503

  @moduledoc """
  This module is responsible for a single api connection.

  It handles incoming api messages and also receives messages from
  the `VoidPhone.Gossip.API` module.

  The typical gossip message flow is as follows when the message is received on the local node:

  1. This module receives a `GOSSIP_ANNOUNCEMENT` message and
     spreads the message locally to all api clients using the `VoidPhone.Gossip.Api.broadcast/6`
     function.
  2. The `VoidPhone.Gossip.Api` sends `{:gossip_notification, global_id, data_type, data}` messages
     to all api clients that are registered in the `VoidPhone.Gossip.NotificationRegistry` for
     the specified `data_type`.
  3. The registered processes receive a `{:gossip_notification, global_id, data_type, data}` message
     and send a `GOSSIP_NOTIFICATION` message to the socket.
  4. The process that originally received the `GOSSIP_ANNOUNCEMENT` message calls the
     `VoidPhone.Gossip.P2p.spread/6` function to spread the message further.

  When the message arrives from another peer, please have a look at the `VoidPhone.Gossip.P2pWorker`
  for a description of the message flow.

  The `GOSSIP_VALIDATION` messages are only used for messages received from another peer.
  We assume that our local api clients are well-behaving and therefore
  spread messages received locally to other peers without waiting for a `GOSSIP_VALIDATION` message.

  Still, for every `GOSSIP_NOTIFICATION` this module expects to receive a `GOSSIP_VALIDATION`
  message. When the `GOSSIP_VALIDATION` response says that the message is invalid it is removed
  from the knowledge base cache. When the message is valid, the `VoidPhone.Gossip.P2p.handle_validation/4`
  is called. If the message was not already spread it is then spread to all connected peers.

  """

  @impl true
  def init({socket, opts}) do
    {:ok, client_addr} = :inet.peername(socket)
    registry = Keyword.get(opts, :registry, NotificationRegistry)
    peer_manager = Keyword.get(opts, :peer_manager, PeerManager)
    knowledge_base = Keyword.get(opts, :knowledge_base, :gossip_knowledge_base)
    validation_cache = Keyword.get(opts, :validation_cache, :gossip_validation_cache)

    # the packetizer is responsible for chunking the incoming data
    # into valid api messages
    # it sends the normal active mode gen_tcp messages:
    # {:tcp, sock, data} or {:tcp_closed, sock} / {:tcp_error, sock, reason}
    {:ok, packetizer} = ApiPacketizer.start_link({socket, target: self()})

    {:ok,
     %{
       registry: registry,
       peer_manager: peer_manager,
       knowledge_base: knowledge_base,
       validation_cache: validation_cache,
       packetizer: packetizer,
       # the actual tcp socket
       socket: socket,
       client_address: client_addr,
       # we keep track of the temporary validation ids
       validation: %{}
     }}
  end

  @impl true
  def handle_info({:tcp, _socket, data}, state = %{packetizer: packetizer}) do
    case handle_message(data, state) do
      :ok ->
        send(packetizer, :recv_new)
        {:noreply, state}

      other ->
        Logger.error("Invalid return code from handle_message: #{inspect(other)}\nExiting!")
        {:stop, :abnormal, state}
    end
  end

  @impl true
  def handle_info({:tcp_closed, _socket}, state) do
    {:stop, :normal, state}
  end

  @impl true
  def handle_info({:tcp_error, _socket, reason}, _state) do
    Process.exit(self(), reason)
  end

  # this callback is invoked when we received a new message for the given data type
  # and we've registered for this message before
  @impl true
  def handle_info({:gossip_notification, global_id, data_type, data}, state = %{socket: socket}) do
    Logger.debug("sending gossip notification to #{inspect(socket)} #{inspect(data)}")
    # create a temporary message id for the verification
    message_id = :rand.uniform(65536) - 1

    message =
      VoidPhone.MessageUtils.build_message(
        @gossip_notification,
        <<message_id::unsigned-integer-size(16), data_type::binary-size(2), data::binary()>>
      )

    :gen_tcp.send(socket, message)

    {
      :noreply,
      # save the global id that identifies this message in the knowledge base
      update_in(state, [:validation], fn validation ->
        Map.put(validation, message_id, global_id)
      end)
    }
  end

  ########### The actual message handling ###########

  # the gossip_announce handler
  defp handle_message(
         <<
           msgtype::integer-size(16),
           ttl::integer-size(8),
           _reserved::binary-size(1),
           data_type::binary-size(2),
           data::binary()
         >>,
         _state = %{
           registry: registry,
           peer_manager: peer_manager,
           knowledge_base: knowledge_base
         }
       )
       when msgtype == @gossip_announce do
    Logger.debug(
      "gossip announce (#{msgtype}) with ttl #{ttl}, data_type: #{inspect(data_type)} and data: #{inspect(data)}"
    )

    case Api.broadcast(data_type, data, ttl, nil, registry, knowledge_base) do
      {:ok, message_id} ->
        # we decrease the ttl for further spreading if and only if it is not zero
        # (which means spread indefinitly)
        ttl =
          case ttl do
            0 -> 0
            other -> other - 1
          end

        # we do not wait for validation, when we received the message locally
        P2p.spread(message_id, ttl, data_type, data, nil, peer_manager)

      {:error, :not_registered} ->
        Logger.debug(
          "Not spreading data, as there is no registered client for #{inspect(data_type)}!"
        )
    end

    :ok
  end

  # the gossip_notify handler
  defp handle_message(
         <<
           msgtype::integer-size(16),
           _reserved::binary-size(2),
           data_type::binary-size(2)
         >>,
         _state = %{registry: registry}
       )
       when msgtype == @gossip_notify do
    Logger.debug("gossip notify request, data_type: #{inspect(data_type)}")

    # register this pid at our notification server, so that we receive
    # an erlang message when a new application message arrives with the given
    # data_type
    Registry.register(registry, data_type, [])

    :ok
  end

  # the gossip_validation handler
  defp handle_message(
         <<
           msgtype::integer-size(16),
           message_id::unsigned-integer-size(16),
           _reserved::size(15),
           valid::size(1)
         >>,
         %{
           validation: validation,
           peer_manager: peer_manager,
           knowledge_base: knowledge_base,
           validation_cache: validation_cache
         }
       )
       when msgtype == @gossip_validation do
    Logger.debug(
      "gossip validation response for message #{inspect(message_id)}, valid: #{inspect(valid)}"
    )

    case Map.get(validation, message_id) do
      nil ->
        {:error, :unexpected_validation}

      global_id when valid == 0 ->
        Cachex.del(knowledge_base, global_id)
        :ok

      global_id when valid == 1 ->
        # the data was validated, allow it to spread further
        P2p.handle_validation(global_id, peer_manager, knowledge_base, validation_cache)
        :ok
    end
  end

  defp handle_message(msg, _state) do
    Logger.warn("Got an unexpected message: #{inspect(msg)}. Closing.")
    {:error, :invalid_message}
  end

  # Client API

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, [])
  end
end
