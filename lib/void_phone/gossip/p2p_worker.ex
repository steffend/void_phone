defmodule VoidPhone.Gossip.P2pWorker do
  use GenServer, restart: :temporary
  require Logger

  #  2. The `VoidPhone.Gossip.Api` creates a message_id and dispatches the message locally to
  # all registered connections in the `VoidPhone.Gossip.NotificationRegistry` for the specified data_type.

  alias VoidPhone.Gossip.{Api, PeerManager, P2pUtils}
  alias VoidPhone.Gossip.{NotificationRegistry, PeerRegistry}

  @moduledoc """
  This module is responsible for a single peer-to-peer connection.

  It handles incoming p2p connections and also receives peer updates from
  the `VoidPhone.Gossip.PeerManager` module.

  The typical gossip p2p message flow is as follows:

  1. The `VoidPhone.Gossip.ApiWorker` on Node A receives a GOSSIP_ANNOUNCEMENT message and
     spreads the message locally to all api clients using the `VoidPhone.Gossip.Api.broadcast/6`
     function.
  2. Upon successful broadcast, the `VoidPhone.Gossip.ApiWorker` calls the `VoidPhone.Gossip.P2p.spread/6`
     which checks the ttl and spreads the message to all nodes that are currently
     registered in the `VoidPhone.Gossip.PeerManager`.
  3. The `{:spread, message_id, ttl, data_type, data}` message is received by this module
     and sent to the socket.
  4. The `handle_message` function on Node B is called with a
     `{:spread, message_id, ttl, data_type, data}` message and Node B
     broadcasts the message locally to all api clients using the `VoidPhone.Gossip.Api.broadcast/6`
     function.
  5. Upon receiving a `GOSSIP_VALIDATION` from any connected api client, the
    `VoidPhone.Gossip.P2p.handle_validation/4` function on Node B is executed which calls the
    `VoidPhone.Gossip.P2p.spread/6` function to spread the message with decreased ttl
     to the nodes that are currently registered in the `VoidPhone.Gossip.PeerManager`.
  6. Go back to step 3, only with the change that it is now Node B that spreads the message.

  """

  @impl true
  def init({socket, opts}) do
    # by default we assume that the peer connected to us
    # this means that we will ask for a proof-of-work challenge, before
    # accepting it as a peer
    p2p_address =
      Keyword.fetch!(opts, :listen_address)
      |> VoidPhone.IpUtils.get_ip_and_port()

    sock_type = Keyword.get(opts, :sock_type, :accepted)
    challenge_difficulty = Keyword.get(opts, :challenge_difficulty, 2)
    challenge_timeout = Keyword.get(opts, :challenge_timeout, 10_000)
    challenge_recv_timeout = Keyword.get(opts, :challenge_recv_timeout, 10_000)

    manager = Keyword.get(opts, :peer_manager, PeerManager)
    api_registry = Keyword.get(opts, :api_registry, NotificationRegistry)
    p2p_registry = Keyword.get(opts, :p2p_registry, PeerRegistry)
    knowledge_base = Keyword.get(opts, :knowledge_base, :gossip_knowledge_base)

    state = %{
      peer_manager: manager,
      api_registry: api_registry,
      p2p_registry: p2p_registry,
      knowledge_base: knowledge_base,
      # the actual tcp socket
      socket: socket,
      # the sock type tells us if we or the peer initiated the connection
      type: sock_type,
      # the p2p address of ourself that we announce
      p2p_address: p2p_address,
      # the address of the remote peer
      peer_address: nil,
      challenge_difficulty: challenge_difficulty,
      challenge_timeout: challenge_timeout,
      challenge_recv_timeout: challenge_recv_timeout,
      challenge_timer: nil,
      # we buffer the random challenge data in the state to verify
      # it when we get a challenge reply
      challenge: nil,
      verified?: false,
      pull_requested: false,
      ping_queue: :queue.new()
    }

    case sock_type do
      :accepted ->
        send(self(), :send_challenge)
        {:ok, state}

      :initiated ->
        # we initiated the connection, therefore
        # we already know the public address of the peer
        with {:ok, address} <- :inet.peername(socket),
             :ok <- PeerManager.register(manager, address, p2p_registry) do
          Logger.metadata(peer: "#{inspect(address)}")

          {:ok,
           state
           |> Map.put(:verified?, true)
           |> Map.put(:peer_address, address)}
        end

      other ->
        Logger.error("invalid sock_type in p2p worker: #{inspect(other)}")
        {:error, :invalid_sock_type}
    end
  end

  @impl true
  def handle_call(:ping, from, state = %{socket: socket}) do
    :gen_tcp.send(socket, :erlang.term_to_binary(:ping))

    {:noreply, Map.update!(state, :ping_queue, fn queue -> :queue.in(from, queue) end)}
  end

  @impl true
  # sent in init when we received the connection and did not initiate it
  def handle_info(
        :send_challenge,
        state = %{
          socket: socket,
          challenge_difficulty: difficulty,
          challenge_recv_timeout: recv_timeout
        }
      ) do
    nonce = :crypto.strong_rand_bytes(16)
    Logger.debug("sending challenge #{inspect(nonce)} with difficulty #{inspect(difficulty)}")

    # we kill idle connections after the configured timeout period
    # (+ a hard coded grace interval of 5 more seconds)
    timer_ref = Process.send_after(self(), :challenge_timeout, recv_timeout)
    :gen_tcp.send(socket, :erlang.term_to_binary({:challenge_request, difficulty, nonce}))

    {:noreply, %{state | challenge: nonce, challenge_timer: timer_ref}}
  end

  @impl true
  def handle_info(:challenge_timeout, state) do
    Logger.info("Peer did not reply to our challenge in time! Bye.")
    {:stop, :normal, state}
  end

  @impl true
  def handle_info({:tcp, _socket, data}, state = %{socket: socket}) do
    try do
      # we use the safe option here to unsafe operations
      # like allocating atoms that a peer could send us
      #
      # note: atoms (like :ok) are never garbage collected
      # and therefore should never be created from unvalidated
      # user input
      data = :erlang.binary_to_term(data, [:safe])

      case handle_message(data, state) do
        :ok ->
          :inet.setopts(socket, active: :once)
          {:noreply, state}

        {:ok, state} when is_map(state) ->
          :inet.setopts(socket, active: :once)
          {:noreply, state}

        other ->
          Logger.error("Invalid return code from handle_message: #{inspect(other)}\nExiting!")
          {:stop, :abnormal, state}
      end
    rescue
      e ->
        Logger.error("decoding p2p data failed with error: #{inspect(e)}. Closing connection!")
        {:stop, :invalid_message, state}
    end
  end

  @impl true
  def handle_info({:tcp_closed, _socket}, state) do
    {:stop, :normal, state}
  end

  @impl true
  def handle_info({:tcp_error, _socket, reason}, _state) do
    Process.exit(self(), reason)
  end

  @impl true
  # this message is received internally from the VoidPhone.Gossip.P2p's spread function
  # we use it to send the p2p spread message to the connected peer
  def handle_info(
        {:spread, message_id, ttl, data_type, data},
        state = %{socket: socket, verified?: verified?}
      ) do
    cond do
      verified? ->
        Logger.debug(
          "spreading message with message id #{message_id} and data_type #{inspect(data_type)}"
        )

        :gen_tcp.send(
          socket,
          :erlang.term_to_binary({:spread, message_id, ttl, data_type, data}) <> "\n"
        )

        {:noreply, state}

      true ->
        {:noreply, state}
    end
  end

  @impl true
  def handle_info({:spread, _message_id, _ttl, _data_type, _data}, state) do
    # peer is not verified yet, it will not receive the message
    {:noreply, state}
  end

  @impl true
  def handle_info(:announce_address, state = %{socket: socket, p2p_address: address}) do
    :gen_tcp.send(socket, :erlang.term_to_binary({:p2p_address, address}))

    {:noreply, state}
  end

  @impl true
  def handle_info(:push_request, state = %{socket: socket}) do
    Logger.debug("pushing push_request to remote")

    :gen_tcp.send(socket, :erlang.term_to_binary(:push_request))

    {:noreply, state}
  end

  @impl true
  def handle_info(:pull_request, state = %{socket: socket}) do
    Logger.debug("sending pull_request to remote")

    :gen_tcp.send(socket, :erlang.term_to_binary(:pull_request))

    {:noreply, %{state | pull_requested: true}}
  end

  defp handle_message(
         {:challenge_request, difficulty, data},
         state = %{socket: socket, challenge_timeout: timeout}
       )
       when is_integer(difficulty) and difficulty > 0 and is_binary(data) and
              byte_size(data) == 16 do
    Logger.debug("received challenge #{inspect(data)} with difficulty #{inspect(difficulty)}")

    pid = self()

    P2pUtils.async_pow(difficulty, data, timeout, fn result ->
      case result do
        {:ok, nonce} ->
          :gen_tcp.send(socket, :erlang.term_to_binary({:challenge_response, data, nonce}))
          # pow success
          # if the challenge is valid the socket will stay open, otherwise the remote peer
          # will close the connection
          # we can go on and send our public p2p address to the peer
          send(pid, :announce_address)

        other ->
          # this will crash the server if the challenge fails
          send(pid, other)
      end
    end)

    {:ok, state}
  end

  defp handle_message({:challenge_request, 0, data}, state = %{socket: socket})
       when is_binary(data) do
    :gen_tcp.send(socket, :erlang.term_to_binary({:challenge_response, data, <<>>}))
    # treat newly connected nodes as if they were received from a p2p push
    send(self(), :announce_address)

    {:ok, state}
  end

  defp handle_message(
         {:challenge_response, data, nonce},
         state = %{
           challenge: requested_data,
           challenge_difficulty: difficulty,
           challenge_timer: challenge_timer
         }
       )
       when data == requested_data and byte_size(nonce) <= 16 do
    # we received a response, cancel the timout
    Process.cancel_timer(challenge_timer)

    if P2pUtils.valid_pow?(difficulty, data, nonce) do
      Logger.debug("peer completed the challenge successfully")
      # treat newly connected nodes as if they were received from a p2p push
      # as the peer id is the public address of the p2p module
      # we announce it now, as the peer completed the challenge successfully
      {:ok, %{state | challenge_timer: nil}}
    else
      {:error, :invalid_pow}
    end
  end

  defp handle_message(
         {:p2p_address, address},
         state = %{peer_manager: manager, p2p_registry: registry, type: :accepted}
       )
       when is_tuple(address) do
    # we received the public peer address, register now @ peer manager
    with :ok <- PeerManager.push(manager, address, registry) do
      Logger.metadata(peer: "#{inspect(address)}")

      {:ok,
       state
       |> Map.put(:verified?, true)
       |> Map.put(:peer_address, address)}
    end
  end

  # a spread request from another peer
  # we spread this data locally and wait for validation
  #
  # if there is no local subscription to the data type, we discard the request
  # and will not spread it further
  #
  # if any client confirms this request as valid, it will be spread further
  # with a decreased ttl
  defp handle_message(
         {:spread, message_id, ttl, data_type, data},
         _state = %{api_registry: registry, knowledge_base: knowledge_base}
       )
       when is_integer(message_id) and is_integer(ttl) and is_binary(data_type) and
              is_binary(data) do
    # received spread request, spread locally and wait for validation
    case Api.broadcast(data_type, data, ttl, message_id, registry, knowledge_base) do
      {:ok, _message_id} ->
        :ok

      {:error, :not_registered} ->
        Logger.debug(
          "p2p: not spreading external data, as there is no registered client for #{inspect(data_type)}!"
        )

        :ok
    end
  end

  # immediately reply ping with pong
  defp handle_message(:ping, _state = %{socket: socket}) do
    :gen_tcp.send(socket, :erlang.term_to_binary(:pong))
  end

  # received the pong; now answer the ping call
  defp handle_message(:pong, state = %{ping_queue: queue}) do
    case :queue.out(queue) do
      {{:value, pid}, queue} ->
        # we used {:noreply, state} in the handle_call
        # now we asynchronously reply to the caller
        GenServer.reply(pid, :pong)
        {:ok, Map.put(state, :ping_queue, queue)}

      {:empty, _queue} ->
        # we should not receive a pong without asking for it
        Logger.error("Received pong without ping!")
        :error
    end
  end

  # we received a push request, forward to peer manager
  defp handle_message(
         :push_request,
         _state = %{peer_manager: manager, peer_address: peer, verified?: verified?}
       ) do
    cond do
      verified? ->
        Logger.debug("got push request")
        PeerManager.push(manager, peer)

      true ->
        :ok
    end
  end

  # we received a pull request, answer with local view
  defp handle_message(
         :pull_request,
         _state = %{peer_manager: manager, socket: socket, verified?: verified?}
       ) do
    cond do
      verified? ->
        Logger.debug("got pull request")

        :gen_tcp.send(
          socket,
          :erlang.term_to_binary({:pull_response, PeerManager.get_peers(manager)})
        )

      true ->
        :ok
    end
  end

  # we received a pull response
  defp handle_message(
         {:pull_response, peer_list},
         state = %{peer_manager: manager, pull_requested: true, verified?: verified?}
       )
       when is_list(peer_list) do
    cond do
      verified? ->
        Logger.debug("got pull response: #{inspect(peer_list)}")

        PeerManager.pull_response(manager, peer_list)

        {:ok, Map.put(state, :pull_requested, false)}

      true ->
        :ok
    end
  end

  defp handle_message(other, _state) do
    Logger.error("received unknown message on p2p connection: #{inspect(other)}")
    # this will cause the match in handle_info above to fail and abort the connection
    :error
  end

  # Client API

  @doc false
  @spec start_link(opts :: tuple()) :: {:ok, pid()} | {:error, reason :: any()}
  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, [])
  end

  @spec ping(pid :: pid(), timeout :: non_neg_integer()) :: :pong
  def ping(pid, timeout \\ 5000) do
    GenServer.call(pid, :ping, timeout)
  end
end
