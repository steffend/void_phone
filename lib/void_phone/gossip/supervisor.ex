defmodule VoidPhone.Gossip.Supervisor do
  # See https://hexdocs.pm/elixir/Supervisor.html
  @moduledoc false

  use Supervisor
  require Logger

  alias VoidPhone.Gossip

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, [], opts)
  end

  defp to_integer(num) when is_integer(num), do: num
  defp to_integer(num) when is_binary(num), do: String.to_integer(num)

  defp to_float(num) when is_float(num), do: num
  defp to_float(num) when is_integer(num), do: num * 1.0
  defp to_float(num) when is_binary(num), do: Float.parse(num) |> elem(0)

  def init(_opts) do
    Logger.info("Starting Gossip...")

    gossip_config = Application.get_env(:void_phone, :gossip, %{})

    api_address = Map.get(gossip_config, "api_address", "127.0.0.1:7001")
    p2p_address = Map.get(gossip_config, "p2p_address", "127.0.0.1:7002")
    bootstrapper = Map.get(gossip_config, "bootstrapper")
    # get cache size from config and map to integer, default to 256
    cache_size = to_integer(Map.get(gossip_config, "cache_size", 256))
    validation_cache_size = to_integer(Map.get(gossip_config, "validation_cache_size", 256))
    p2p_max_peers = to_integer(Map.get(gossip_config, "max_connections", 32))
    p2p_challenge_difficulty = to_integer(Map.get(gossip_config, "p2p_challenge_difficulty", 2))
    p2p_challenge_timeout = to_integer(Map.get(gossip_config, "p2p_challenge_timeout", 10000))

    p2p_challenge_recv_timeout =
      to_integer(Map.get(gossip_config, "p2p_challenge_recv_timeout", 10000))

    brahms_push_interval = to_integer(Map.get(gossip_config, "brahms_push_interval", 30))
    brahms_pull_interval = to_integer(Map.get(gossip_config, "brahms_pull_interval", 30))
    brahms_update_interval = to_integer(Map.get(gossip_config, "brahms_update_interval", 60))

    brahms_sampler_check_interval =
      to_integer(Map.get(gossip_config, "brahms_sampler_check_interval", 60))

    brahms_alpha = to_float(Map.get(gossip_config, "brahms_alpha", 0.45))
    brahms_beta = to_float(Map.get(gossip_config, "brahms_beta", 0.45))
    brahms_gamma = to_float(Map.get(gossip_config, "brahms_gamma", 0.1))

    if Enum.sum([brahms_alpha, brahms_beta, brahms_gamma]) != 1.0 do
      Logger.error(
        ~s[The configuration parameters "brahms_alpha", "brahms_beta" and "brahms_gamma" must sum to 1.]
      )

      raise "invalid configuration"
    end

    children = [
      # the knowledge base cache
      Supervisor.child_spec({Cachex, name: :gossip_knowledge_base, limit: cache_size},
        id: VoidPhone.Gossip.KnowledgeCache
      ),
      # the cache for handling validations of external gossip announcements
      Supervisor.child_spec(
        {Cachex, name: :gossip_validation_cache, limit: validation_cache_size},
        id: VoidPhone.Gossip.ValidationCache
      ),
      # https://hexdocs.pm/elixir/1.12/Registry.html
      {Registry, keys: :duplicate, name: VoidPhone.Gossip.NotificationRegistry},
      # our peer registry
      {Registry, keys: :unique, name: VoidPhone.Gossip.PeerRegistry},
      # Client API
      {Gossip.Api,
       listen_address: api_address,
       knowledge_base: :gossip_knowledge_base,
       validation_cache: :gossip_validation_cache},
      {DynamicSupervisor, strategy: :one_for_one, name: VoidPhone.Gossip.ApiSupervisor},
      # P2p API
      {Gossip.P2p,
       listen_address: p2p_address,
       challenge_difficulty: p2p_challenge_difficulty,
       challenge_timeout: p2p_challenge_timeout,
       challenge_recv_timeout: p2p_challenge_recv_timeout},
      {DynamicSupervisor, strategy: :one_for_one, name: VoidPhone.Gossip.P2pSupervisor},
      # the peer manager connects to peers
      {Gossip.PeerManager,
       bootstrapper: bootstrapper,
       address: p2p_address,
       max_peers: p2p_max_peers,
       alpha: brahms_alpha,
       beta: brahms_beta,
       gamma: brahms_gamma,
       push_interval: brahms_push_interval,
       pull_interval: brahms_pull_interval,
       update_interval: brahms_update_interval,
       sampler_check_interval: brahms_sampler_check_interval}
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
