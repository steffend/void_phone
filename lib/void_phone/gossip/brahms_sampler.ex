defmodule VoidPhone.Gossip.BrahmsSampler do
  @moduledoc """
  This module implements a sampler as described in the lecture notes
  for the Brahms algorithm.

  Instead of using different hash functions for each sampler,
  this implementation creates a new seed for every sampler,
  but always uses sha256 for the actual hashing.
  """

  defstruct seed: nil,
            element: nil

  alias VoidPhone.Gossip.BrahmsSampler, as: Sampler

  # creates the actual data that we feed into the hash function
  defp hash_data(seed, element) do
    element = :erlang.term_to_binary(element)
    <<seed::binary(), element::binary(), seed::binary()>>
  end

  @doc """
  Creates a new sampler.

  ## Examples

      iex> VoidPhone.Gossip.BrahmsSampler.new(<<0>>)
      %VoidPhone.Gossip.BrahmsSampler{element: nil, seed: <<0>>}

      # generates a random seed when no seed is given
      iex> state = VoidPhone.Gossip.BrahmsSampler.new()
      iex> is_binary(state.seed)
      true

  """
  @spec new(seed :: binary()) :: %Sampler{}
  def new(seed \\ :crypto.strong_rand_bytes(16)) do
    %Sampler{seed: seed}
  end

  @doc """
  Updates the sampler's state, potentially updating the sampled element.

  ## Examples

      iex> state = VoidPhone.Gossip.BrahmsSampler.new(<<0>>)
      iex> VoidPhone.Gossip.BrahmsSampler.next(state, [:foo, :baz, :bar])
      %VoidPhone.Gossip.BrahmsSampler{element: :baz, seed: <<0>>}

  """
  @spec next(sampler :: %Sampler{}, elements :: term() | list(term())) :: %Sampler{}
  def next(sampler, elements)

  def next(%Sampler{} = sampler, elements) when is_list(elements) do
    # apply all elements to the sampler
    Enum.reduce(elements, sampler, fn element, current_sampler ->
      next(current_sampler, element)
    end)
  end

  def next(%Sampler{element: nil} = state, elem) do
    Map.put(state, :element, elem)
  end

  def next(%Sampler{seed: seed, element: q} = state, elem) do
    cond do
      :crypto.hash(:sha256, hash_data(seed, elem)) < :crypto.hash(:sha256, hash_data(seed, q)) ->
        Map.put(state, :element, elem)

      true ->
        state
    end
  end

  @doc """
  Given a sampler state, returns the current sample.

  ## Examples

      iex> state = VoidPhone.Gossip.BrahmsSampler.new()
      iex> state = VoidPhone.Gossip.BrahmsSampler.next(state, "myid")
      iex> VoidPhone.Gossip.BrahmsSampler.sample(state)
      "myid"
  """
  @spec sample(sampler :: %Sampler{}) :: term()
  def sample(%Sampler{element: e}), do: e
end
