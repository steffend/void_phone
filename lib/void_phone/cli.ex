defmodule VoidPhone.CLI do
  @moduledoc """
  This script provides an entrypoint into the VoidPhone Gossip server.

  It accepts the `--config` (or short `-c`) parameter, as required by the project specification.

  ## Creating the script

  The script can be built by running `mix escript.build`. This will create a new executable
  called `void_phone` in the current working directory. Please note that running an escript
  still requires Erlang to be installed on the system. See the [README](/overview.html)
  for details about running the project.

  """

  # Starts the VoidPhone application after parsing the specified config file.
  defp run() do
    Application.ensure_all_started(:void_phone)
    # this is an escript that would terminate when finished
    Process.sleep(:infinity)
  end

  defp print_usage() do
    IO.puts("Usage: #{:escript.script_name()} -c path_to_config.ini")
  end

  @doc """
  The main entrypoint for the escript.
  """
  @spec main(args :: list()) :: any()
  def main(args) do
    with {:ok, _opts} <- load_config(args) do
      run()
    else
      {:error, :no_config_file} -> print_usage()
    end
  end

  @spec load_config(args :: list()) :: {:ok, keyword()} | {:error, :no_config_file}
  def load_config(args) do
    options = [
      switches: [config: :string, loglevel: :string],
      aliases: [c: :config, l: :loglevel]
    ]

    {opts, _, _} = OptionParser.parse(args, options)

    if level = Keyword.get(opts, :loglevel) do
      Application.put_env(:logger, :level, String.to_existing_atom(level))
    end

    if config_path = Keyword.get(opts, :config) do
      # parse the ini-like config and put the results into the application environment
      VoidPhone.ConfigParser.parse(config_path)
      |> Enum.each(fn
        {key, value} when is_binary(key) ->
          # we assume that the config does not contain more than `:erlang.system_info(:atom_limit)`
          Application.put_env(:void_phone, String.to_atom(key), value)

        {key, value} when is_atom(key) ->
          Application.put_env(:void_phone, key, value)
      end)

      {:ok, opts}
    else
      {:error, :no_config_file}
    end
  end
end
