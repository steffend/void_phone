defmodule VoidPhone.IpUtils do
  @doc """
  Parses the listen address as specified in the project specification.

  ## Examples

      iex> VoidPhone.IpUtils.get_ip_and_port("127.0.0.1:8080")
      {{127, 0, 0, 1}, 8080}

      iex> VoidPhone.IpUtils.get_ip_and_port("[::1]:8080")
      {{0, 0, 0, 0, 0, 0, 0, 1}, 8080}

      iex> VoidPhone.IpUtils.get_ip_and_port("localhost:8080")
      {{127, 0, 0, 1}, 8080}

  """
  @spec get_ip_and_port(listen_address :: String.t()) :: {tuple(), integer()}
  def get_ip_and_port(listen_address) do
    [hostname, port] =
      listen_address
      |> String.reverse()
      |> String.split(":", parts: 2)
      |> Enum.reverse()
      |> Enum.map(&String.reverse/1)

    cond do
      Regex.match?(
        ~r/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/,
        hostname
      ) ->
        # ipv4 address
        {:ok, ip} = :inet.parse_address(hostname |> String.to_charlist())
        {ip, String.to_integer(port)}

      # https://stackoverflow.com/questions/53497/regular-expression-that-matches-valid-ipv6-addresses
      Regex.match?(
        ~r/^\[(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))\]$/,
        hostname
      ) ->
        # ipv6 address
        ip_string = String.slice(hostname, 1, String.length(hostname) - 2)
        {:ok, ip} = :inet.parse_address(ip_string |> String.to_charlist())
        {ip, String.to_integer(port)}

      true ->
        # I guess that it's a hostname then?
        {:ok, {:hostent, _, _, :inet, _, [ip | _]}} =
          :inet.gethostbyname(hostname |> String.to_charlist())

        {ip, String.to_integer(port)}
    end
  end
end
