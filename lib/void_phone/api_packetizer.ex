defmodule VoidPhone.ApiPacketizer do
  use GenServer, restart: :temporary
  require Logger

  @moduledoc """
  This module packages void_phone API messages.

  Erlang's `:gen_tcp` has some great built-in packaging modes, but
  sadly the API protocol does not quite match the built-in two byte
  length prefix mode, as the message type is also included in the header length.
  Therefore this module implements a custom packetizer that uses the correct length.

  ## Usage

  The following example shows how the packetizer can be used in a standard
  GenServer that gets intialized with a `:gen_tcp` socket:

      defmodule MyVoidPhoneApiWorker do
        use GenServer, restart: :temporary

        @impl true
        def init(socket) do
          {:ok, packetizer} = ApiPacketizer.start_link({socket, target: self()})

          {:ok, %{socket: socket, packetizer: packetizer}}
        end

        @impl true
        def handle_info({:tcp, _socket, data}, state = %{packetizer: packetizer}) do
          try do
            case handle_message(data, state) do
              :ok ->
                # tell the packetizer to receive the next message
                # used for back-pressure
                send(packetizer, :recv_new)
                {:noreply, state}

              other ->
                {:stop, :abnormal, state}
            end
          rescue
            e ->
              {:stop, :invalid_message, state}
          end
        end

        @impl true
        def handle_info({:tcp_closed, _socket}, state) do
          {:stop, :normal, state}
        end

        @impl true
        def handle_info({:tcp_error, _socket, reason}, _state) do
          Process.exit(self(), reason)
        end

        # a handler for the api message
        defp handle_message(<<msgtype::integer-size(16), data::binary>>), do: :ok
      end

  You need to handle the normal `:gen_tcp` active mode messages:
    * `{:tcp, socket, data}`
    * `{:tcp_closed, socket}`
    * `{:tcp_error, socket, reason}`

  For a concrete example of an implementation, see the `VoidPhone.Gossip.ApiWorker` module.
  """

  @impl true
  def init({socket, opts}) do
    target = Keyword.fetch!(opts, :target)
    # prevents clients from sending a header and then never replying with the actual data
    message_recv_timeout = Keyword.get(opts, :message_recv_timeout, :timer.seconds(30))

    {:ok,
     %{
       socket: socket,
       target: target,
       message_recv_timeout: message_recv_timeout
     }, {:continue, :receive_message}}
  end

  defp receive_body(_socket, 0, _timeout), do: {:ok, <<>>}
  defp receive_body(socket, size, timeout), do: :gen_tcp.recv(socket, size, timeout)

  @impl true
  def handle_continue(
        :receive_message,
        state = %{socket: socket, target: target, message_recv_timeout: timeout}
      ) do
    # start by receiving the message header
    with {:ok, <<size::integer-size(16), msgtype::integer-size(16)>>} <- :gen_tcp.recv(socket, 4),
         # we received the header, now wait for the message body to arrive
         {:ok, data} <- receive_body(socket, size - 4, timeout) do
      # we only send complete messages to the worker,
      # therefore we do not include the size from the header
      send(target, {:tcp, socket, <<msgtype::integer-size(16), data::binary()>>})
    else
      {:error, :closed} -> send(target, {:tcp_closed, socket})
      {:error, reason} -> send(target, {:tcp_error, socket, reason})
    end

    # return to the genserver loop; when there was an error
    # the worker will exit and as this process is linked,
    # it will die too.
    {:noreply, state}
  end

  @impl true
  def handle_info(:recv_new, state) do
    # basically the same as :inet.setopts(socket, active: :once)
    # when using :gen_tcp;
    # we do not want to flood the worker with messages
    # therefore it has to explicitly tell us when it is ready
    # to receive more data
    {:noreply, state, {:continue, :receive_message}}
  end

  ## Client API ##

  @spec start_link({socket :: port(), opts :: keyword()}) ::
          {:ok, pid()} | {:error, reason :: any()}
  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts)
  end
end
