defmodule Mix.Tasks.Register do
  @shortdoc "Runs the registration procedure for p2psec."
  @moduledoc """
  This module provides a basic mix task for registering a new project with the p2psec server.

  You can run this on your cli by invoking `mix register` with the following arguments:

  * `--email` your email address
  * `--firstname` your first name
  * `--lastname` your last name
  * `--lrz` your gitlab lrz username (without leading @)
  * `--project` one of `["gossip", "dht", "rps", "nse", "onion"]` or the special value "idonotexist"
    for triggering a failure.

  Please note that depending on your luck, the execution of this task can vary
  as we have to perform a proof of work algorithm.
  """

  use Mix.Task
  require Logger

  @enroll_init 680
  @enroll_register 681
  @enroll_success 682
  @enroll_failure 683

  @project_map %{
    "gossip" => 2961,
    "dht" => 4963,
    "rps" => 15882,
    "nse" => 7071,
    "onion" => 39943,
    "idonotexist" => 666
  }

  defp connect(host, port) do
    :gen_tcp.connect(host, port, [:binary, active: false, reuseaddr: true])
  end

  # a convenient wrapper around gen_tcp.recv that directly parses the received
  # messages and parses them into the correct format using `handle_message/1`.
  defp wait_and_handle(socket) do
    {:ok, data} = :gen_tcp.recv(socket, 0)
    handle_message(data)
  end

  # these functions perform binary pattern matching
  # (see https://hexdocs.pm/elixir/Kernel.SpecialForms.html#%3C%3C%3E%3E/1
  # or if you're more into tutorial, read https://zohaib.me/binary-pattern-matching-in-elixir/)
  defp handle_message(
         <<
           size::integer-size(16),
           msgtype::integer-size(16),
           challenge::size(64)
         >> = msg
       )
       when byte_size(msg) == size
       when msgtype == @enroll_init do
    {:ok, {:challenge, challenge}}
  end

  defp handle_message(
         <<
           size::integer-size(16),
           msgtype::integer-size(16),
           _reserved::binary-size(2),
           team_number::integer-size(16)
         >> = msg
       )
       when msgtype == @enroll_success and byte_size(msg) == size do
    {:ok, {:team_number, team_number}}
  end

  defp handle_message(
         <<
           size::integer-size(16),
           msgtype::integer-size(16),
           _reserved::binary-size(2),
           error_number::integer-size(16),
           error_description::binary()
         >> = msg
       )
       when msgtype == @enroll_failure and byte_size(msg) == size do
    {:error, {error_number, error_description}}
  end

  defp handle_message(other) do
    {:error, {:invalid_message, other}}
  end

  # builds a correct registration body and encodes the necessary
  # parameters with carriage return + line feed
  defp build_register_msg(challenge, team_nr, project_nr, nonce, email, firstname, lastname, lrz) do
    encoded_params = "#{email}\r\n#{firstname}\r\n#{lastname}\r\n#{lrz}"

    <<challenge::size(64), team_nr::integer-size(16), project_nr::integer-size(16),
      nonce::binary-size(8), encoded_params::binary()>>
  end

  # recursively tries to create a nonce with the first 3 bytes of the resulting sha256 hash set to 0
  defp calc_pow(
         difficulty,
         target,
         attempt,
         challenge,
         team_nr,
         project_nr,
         email,
         firstname,
         lastname,
         lrz
       ) do
    nonce = <<attempt::integer-size(64)>>

    msg =
      build_register_msg(challenge, team_nr, project_nr, nonce, email, firstname, lastname, lrz)

    hash = :crypto.hash(:sha256, msg)

    case hash do
      <<0::size(difficulty)-unit(8), _rest::binary()>> ->
        # yay, finally we are done!
        send(target, {:nonce, nonce})

      _other ->
        # try again...
        calc_pow(
          difficulty,
          target,
          attempt + 1,
          challenge,
          team_nr,
          project_nr,
          email,
          firstname,
          lastname,
          lrz
        )
    end
  end

  # peforms the actual registration procedure:
  # calculates the nonce for the specified parameters, sends the message on the socket
  # and waits for the reply
  defp register(
         socket,
         {difficulty, challenge, team_nr, project_nr, email, firstname, lastname, lrz}
       ) do
    Logger.info("Registering with #{inspect(binding())}")

    target = self()

    tasks =
      Enum.map(1..System.schedulers_online(), fn _ ->
        # Start a hashing task on each scheduler with a unique random seed
        Task.async(fn ->
          calc_pow(
            difficulty,
            target,
            :rand.uniform(10_000_000_000),
            challenge,
            team_nr,
            project_nr,
            email,
            firstname,
            lastname,
            lrz
          )
        end)
      end)

    nonce =
      receive do
        {:nonce, nonce} ->
          # we found a nonce, kill all other tasks
          Enum.each(tasks, fn task -> Task.shutdown(task, :brutal_kill) end)
          nonce
      after
        10_000 -> raise "timeout calculating nonce, woops!"
      end

    msg =
      build_register_msg(challenge, team_nr, project_nr, nonce, email, firstname, lastname, lrz)

    # we add 4 here as the length consists of payload AND header, with 2 bytes for the length
    # and two more bytes for the message type
    msg_length = byte_size(msg) + 4

    :ok =
      :gen_tcp.send(
        socket,
        <<msg_length::integer-size(16), @enroll_register::integer-size(16), msg::binary()>>
      )

    wait_and_handle(socket)
  end

  @impl true
  @doc """
  The main entry point for this mix task. For actual documentation on how to use this, see
  the module documentation at the beginning.
  """
  def run(argv) do
    {parsed, _other} =
      OptionParser.parse!(argv,
        strict: [
          host: :string,
          port: :integer,
          email: :string,
          firstname: :string,
          lastname: :string,
          lrz: :string,
          project: :string,
          team: :integer,
          difficulty: :integer
        ]
      )

    Logger.info("Running with arguments: #{inspect(parsed)}")

    host = Keyword.get(parsed, :host, "p2psec.net.in.tum.de") |> String.to_charlist()
    port = Keyword.get(parsed, :port, 13337)
    team_nr = Keyword.get(parsed, :team, 0)
    project = Keyword.fetch!(parsed, :project)
    project_nr = Map.get(@project_map, project)
    firstname = Keyword.fetch!(parsed, :firstname)
    lastname = Keyword.fetch!(parsed, :lastname)
    email = Keyword.fetch!(parsed, :email)
    lrz = Keyword.fetch!(parsed, :lrz)
    difficulty = Keyword.get(parsed, :difficulty, 3)

    with {:ok, socket} <- connect(host, port),
         {:ok, {:challenge, challenge}} <- wait_and_handle(socket),
         _ <- Logger.info("Received the challenge: #{inspect(challenge)}"),
         {:ok, {:team_number, number}} <-
           register(
             socket,
             {difficulty, challenge, team_nr, project_nr, email, firstname, lastname, lrz}
           ) do
      Logger.info("Successfully registered as team #{inspect(number)}! Please note that number!")
    else
      {:error, {code, description}} ->
        Logger.error(
          "Register failed with code: #{inspect(code)} and description: #{inspect(description)}"
        )

      other ->
        Logger.error("We did not expect this: #{inspect(other)}")
    end
  end
end
