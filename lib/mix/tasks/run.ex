defmodule Mix.Tasks.RunServer do
  @shortdoc "Runs the server with the specified arguments."
  @moduledoc """
  This module provides a simple task for running the VoidPhone server without building an escript first.
  Normally, you would create a packaged script by running `mix escript.build` and then starting the server
  with e.g. `./void_phone -c config.ini`.

  This task allows you to run it with `mix run_server -c config.ini`, or even more
  conveniently in an interactive elixir session using `iex -S mix run_server -c config.ini`.
  """

  use Mix.Task

  @impl true
  def run(args) do
    with {:ok, opts} <- VoidPhone.CLI.load_config(args) do
      filtered_args =
        opts
        |> Keyword.delete(:config)
        |> Keyword.delete(:loglevel)
        |> OptionParser.to_argv()

      Mix.Tasks.Run.run(run_args() ++ filtered_args)
    else
      {:error, :no_config_file} -> Mix.shell().error("Usage: mix run_server -c config.ini")
    end
  end

  defp run_args() do
    if iex_running?(), do: [], else: ["--no-halt"]
  end

  defp iex_running?() do
    Code.ensure_loaded?(IEx) and IEx.started?()
  end
end
