FROM hexpm/elixir:1.12.3-erlang-24.0.6-alpine-3.14.0 as build

LABEL maintainer="Steffen Deusch"

ENV MIX_ENV=prod

RUN mix local.rebar --force \
    && mix local.hex --force

WORKDIR /app

# copy the mix project definition
COPY ./mix.exs mix.exs
RUN mix deps.get
# copy the actual app
COPY ./config config
COPY ./lib lib
# build the script
RUN mix escript.build

# app stage
FROM hexpm/erlang:24.0.6-alpine-3.14.0

RUN apk add --update --no-cache tini

# install void_phone script
COPY --from=build /app/void_phone /usr/bin/void_phone

# define default commands
ENTRYPOINT ["tini", "--", "/usr/bin/void_phone"]
