#!/usr/bin/env bash

# change the ports below if not running on linux

docker build -t void_phone -f Dockerfile .

if [ "$(uname -s)" == "Linux" ]; then
    docker run \
        --rm \
        --net=host \
        -v `pwd`:`pwd` \
        -w `pwd` \
        -it void_phone $@
else
    echo
    echo "Not running on Linux!"
    echo "Please modify the port mappings used when starting the container in this script!"
    echo
    # modify -p 7001:7001 and 7002:7002 below or add more mappings
    docker run \
        --rm \
        -v `pwd`:`pwd` \
        -w `pwd` \
        -p 7001:7001 \
        -p 7002:7002 \
        -it void_phone $@
fi
