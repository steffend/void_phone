#!/usr/bin/env bash

docker build -t void_phone_demo -f Dockerfile.demo .

if [ "$(uname -s)" == "Linux" ]; then
    docker run \
        --rm \
        --net=host \
        -v `pwd`:`pwd` \
        -w `pwd` \
        -e VOID_PHONE_CONFIG="${1:-/tmp/config.ini}" \
        -e PORT="${2:-7000}" \
        -e SECRET_KEY_BASE="CC9693548BD0D6E76588441A62FD8FA53D2B09FACDA74B93F15E687B5AAE8035" \
        -it void_phone_demo
else
    echo
    echo "Not running on Linux!"
    echo "Please modify the port mapping used when starting the container in this script!"
    echo
    # modify -p 7000:7000 below or add more mappings
    docker run \
        --rm \
        -v `pwd`:`pwd` \
        -w `pwd` \
        -p 7000:7000 \
        -e VOID_PHONE_CONFIG="${1:-/tmp/config.ini}" \
        -e PORT="${2:-7000}" \
        -e SECRET_KEY_BASE="CC9693548BD0D6E76588441A62FD8FA53D2B09FACDA74B93F15E687B5AAE8035" \
        -it void_phone_demo
fi
