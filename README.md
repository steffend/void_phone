# VoidPhone

This is the repository for the Gossip implementation of the VoidPhone application. You are looking at the repository of Team 5. The documentation, including the reports, is available [here](https://netintum.pages.gitlab.lrz.de/teaching/p2psec_projects_2021/Gossip-5). See [Documentation](#documentation) on how the documentation is built.

## Configuration

The project expects a windows ini-like configuration file as described in the project specification.
The following configuration options are used by this implementation:

```ini
[gossip]
cache_size = 3
validation_cache_size = 256
max_connections = 30
bootstrapper = localhost:8002
api_address = localhost:7001
p2p_address = localhost:7002
p2p_challenge_difficulty = 2
p2p_challenge_timeout = 1000
p2p_challenge_recv_timeout = 10000
brahms_alpha = 0.45
brahms_beta = 0.45
brahms_gamma = 0.1
brahms_push_interval = 5
brahms_pull_interval = 5
brahms_update_interval = 10
brahms_sampler_check_interval = 60
```

* `cache_size` The maximum number of values in the local knowledge base. The knowledge base is used for temporarily storing messages while they are being validated by the api connections.
* `validation_cache_size` The maximum number of values in the validation cache. A message id is cached in there when a validation message arrives to prevent spreading it multiple times in the case that multiple api clients validate the message.
* `max_connections` The maximum number of peers the implementation will try to connect to.
* `bootstrapper` The `p2p_address` of a node that should be used as bootstrapper.
* `api_address` As described in the project specification. This will be parsed accordingly by the `VoidPhone.IpUtils.get_ip_and_port/1` function.
* `p2p_address` As described in the project specification.
* `p2p_challenge_difficulty` How many leading zero-bytes are required in the p2p proof-of-work challenge. A typical value is two or three. You can use 0 to disable the challenge.
* `p2p_challenge_timeout` To prevent malicious nodes from requiring an arbitrarily complex challenge, you can define a timeout for performing the challenge. This value is expected to be in milliseconds.
* `p2p_challenge_recv_timeout` To prevent waiting forever on challenge responses, you can define a timeout for clients completing the challenge. This value is expected to be in milliseconds.
* `brahms_alpha` The proportion of push values when performing a view update.
* `brahms_beta` The proportion of pull values when performing a view update.
* `brahms_gamma` The proportion of history samples when performing a view update; alpha, beta and gamma must sum to one.
* `brahms_push_interval` The number of seconds between two consecutive push requests.
* `brahms_pull_interval` The number of seconds between two consecutive pull requests.
* `brahms_update_interval` The number of seconds between two consecutive view updates (should be larger than the previous two values).
* `brahms_sampler_check_interval` The number of seconds between checking if sampled peers are still alive.

## Running the project

tl;dr use `run.sh` for running with Docker. For setting up a multi-node environment, see [Setting up multiple peers using docker-compose](docs/docker_compose.md).

### Using Docker

Both ways generally expect the path to the configuration file using `-c` (or `--config`).
Optionally, the log level can be changed using `-l` (or `--loglevel`) and accepts the loglevels as documented for the [Elixir Logger](https://hexdocs.pm/logger/1.12.3/Logger.html#module-levels).

When not having Elixir installed locally, the project can be run inside of a Docker container using the following commands, assuming the current working directory is the project and ports 7001 / 7002 should be used for api / p2p connections:

```bash
$ docker build -t void_phone .
$ docker run --rm -p 7001:7001 -p 7002:7002 -v /path/to/config.ini:/tmp/config.ini void_phone -c /tmp/config.ini -l info
```

or simply run the `run.sh` script, e.g. `./run.sh -c config.ini`.
To access an interactive Elixir shell, you can use `./run_interactive.sh -c config.ini`.
See [inspecting a node](#inspecting-a-node) for some examples using the Elixir IEx shell.

> Warning for non-Linux users:
Please make sure to map the API and P2P ports correctly. When running on Linux, you can use `--net=host` to use host networking for the container. This is **NOT** supported on macOS (and probably also not on Windows), so please make sure that the addresses specified in the config file use `host.docker.internal` instead of `localhost` or `127.0.0.1` when accessing the host machine in that case. Also make sure to modify the port mappings using `-p` in the commands or scripts used.

Please note that the container currently does not exit when sending SIGINT (Ctrl+C). To stop it, run `docker stop` with the corresponding container id.

### Locally

When Elixir (min version: 1.12) and Erlang (min version: 24) are installed locally:

1. Install the project dependencies using the command: `mix deps.get`
2. Run the module using either `mix run_server -c config.ini` or `iex -S mix run_server -c config.ini` with the latter option also giving an interactive Elixir shell.
 
There is also the option to generate an [escript](https://hexdocs.pm/mix/1.12/Mix.Tasks.Escript.Build.html), that generates an executable that only needs [Erlang](https://www.erlang.org) installed on the local machine, but using Docker is typically more practical.
## Notes

* The gossip implementation always spreads information from local api connection to all connected peers.
* Messages that are received from another peer will only be forwarded, once at least one api connection validated the message.

## Documentation

This project uses [ex_doc](https://hexdocs.pm/ex_doc/readme.html) to document the code. You can generate the documentation by running `mix docs` or using the `build_docs.sh` script (requires docker and bash!). This will create an HTML (and an epub) version of the documentation under `doc/`.

Alternatively, the docs are also deployed by the GitLab CI pipeline to the [LRZ GitLab Pages](https://netintum.pages.gitlab.lrz.de/teaching/p2psec_projects_2021/Gossip-5/).

## Tests

Running the tests can be achieved either by using `mix test` or `mix test --cover` when Elixir is installed, or by using the `./test.sh` script in the project root (uses Docker).

## Demo Application

There exists a demo application that uses Gossip to relay messages across browsers. The app is located in the `gossip_demo` folder and can be started in the following ways:

* with Elixir installed: `cd gossip_demo && mix deps.get && sh -c 'VOID_PHONE_CONFIG=../config.ini PORT=7000 iex -S mix phx.server'`
* with Docker: `./run_demo.sh config.ini 7000` with the first argument the path to the void_phone config file and the second argument the port

Optionally (may not work in the future), you can have a look at the hosted demo sites:
* https://peer-a.voidphone.steffend.me
* https://peer-b.voidphone.steffend.me
