#!/usr/bin/env bash

docker run --rm \
    -v `pwd`:`pwd` \
    -w `pwd` \
    -ti \
    hexpm/elixir:1.12.3-erlang-24.0.6-alpine-3.14.0 \
    sh -c "mix local.hex --force && mix local.rebar --force && mix deps.get && mix test --cover"
