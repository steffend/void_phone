defmodule VoidPhone.CLITest do
  use ExUnit.Case, async: false

  import ExUnit.CaptureIO

  test "fails when no config file" do
    assert capture_io(fn -> VoidPhone.CLI.main(["-l", "debug"]) end) =~ "Usage:"
  end
end
