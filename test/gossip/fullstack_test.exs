defmodule VoidPhone.Gossip.FullstackTest do
  use ExUnit.Case, async: true

  import Liveness

  def setup_peer(peername) do
    peer_manager = Module.concat(__MODULE__, "PeerManager_#{peername}")
    p2p_supervisor = Module.concat(__MODULE__, "P2pSupervisor_#{peername}")
    api_supervisor = Module.concat(__MODULE__, "ApiSupervisor_#{peername}")
    notification_registry = Module.concat(__MODULE__, "NotificationRegistry_#{peername}")
    peer_registry = Module.concat(__MODULE__, "PeerRegistry_#{peername}")
    knowledge_base = Module.concat(__MODULE__, "KnowledgeBase_#{peername}")
    validation_cache = Module.concat(__MODULE__, "ValidationCache_#{peername}")

    {:ok, _} =
      start_supervised(
        Supervisor.child_spec({Cachex, name: knowledge_base, limit: 32},
          id: knowledge_base
        )
      )

    {:ok, _} =
      start_supervised(
        Supervisor.child_spec(
          {Cachex, name: validation_cache, limit: 32},
          id: validation_cache
        )
      )

    {:ok, _} = start_supervised({DynamicSupervisor, strategy: :one_for_one, name: p2p_supervisor})
    {:ok, _} = start_supervised({DynamicSupervisor, strategy: :one_for_one, name: api_supervisor})

    {:ok, _} = start_supervised({Registry, keys: :duplicate, name: notification_registry})
    {:ok, _} = start_supervised({Registry, keys: :unique, name: peer_registry})

    {:ok, p2p_socket} = :gen_tcp.listen(0, [:binary, packet: 2, active: :once, reuseaddr: true])
    {:ok, {_, p2p_port}} = :inet.sockname(p2p_socket)

    {:ok, api_socket} =
      :gen_tcp.listen(0, [:binary, packet: :raw, active: false, reuseaddr: true])

    {:ok, {_, api_port}} = :inet.sockname(api_socket)

    {:ok, _} =
      start_supervised(
        Supervisor.child_spec(
          {VoidPhone.Gossip.Api,
           listen_socket: api_socket,
           listen_address: "127.0.0.1:#{api_port}",
           supervisor: api_supervisor,
           registry: notification_registry,
           peer_manager: peer_manager,
           knowledge_base: knowledge_base,
           validation_cache: validation_cache},
          id: Module.concat(__MODULE__, "ApiTcp_#{peername}")
        )
      )

    {:ok, _} =
      start_supervised(
        Supervisor.child_spec(
          {VoidPhone.Gossip.P2p,
           listen_socket: p2p_socket,
           listen_address: "127.0.0.1:#{p2p_port}",
           challenge_difficulty: 1,
           challenge_timeout: 200,
           challenge_recv_timeout: 200,
           supervisor: p2p_supervisor,
           api_registry: notification_registry,
           peer_manager: peer_manager,
           knowledge_base: knowledge_base},
          id: Module.concat(__MODULE__, "P2pTcp_#{peername}")
        )
      )

    {:ok, _} =
      start_supervised(
        Supervisor.child_spec(
          {
            VoidPhone.Gossip.PeerManager,
            # instantly retry
            name: peer_manager,
            registry: peer_registry,
            api_registry: notification_registry,
            supervisor: p2p_supervisor,
            bootstrapper: nil,
            address: "127.0.0.1:#{p2p_port}",
            max_peers: 3,
            push_interval: 600,
            pull_interval: 600,
            update_interval: 1200,
            sampler_check_interval: 600,
            sampler_check_ping_timeout: 50
          },
          id: peer_manager
        )
      )

    %{
      peer_manager: peer_manager,
      peer_registry: peer_registry,
      p2p_supervisor: p2p_supervisor,
      api_supervisor: api_supervisor,
      notification_registry: notification_registry,
      p2p_socket: p2p_socket,
      p2p_port: p2p_port,
      api_socket: api_socket,
      api_port: api_port,
      knowledge_base: knowledge_base,
      validation_cache: validation_cache
    }
  end

  defp get_api_worker(api_supervisor, port) do
    eventually(fn ->
      DynamicSupervisor.which_children(api_supervisor)
      |> then(fn children ->
        assert length(children) > 0
        children
      end)
      |> Enum.map(fn {_, pid, _, _} -> pid end)
      |> Enum.find(fn pid ->
        match?(%{client_address: {{127, 0, 0, 1}, ^port}}, :sys.get_state(pid))
      end)
      |> then(fn pid ->
        assert is_pid(pid)
        pid
      end)
    end)
  end

  defp flush_queues(worker) do
    eventually(fn ->
      assert {_, 0} = :erlang.process_info(worker, :message_queue_len)
    end)
  end

  setup do
    # setup all the required processes and servers
    # to run isolated from other tests
    # by using ephemeral ports for all servers
    {:ok, %{peer_1: setup_peer("A"), peer_2: setup_peer("B"), peer_3: setup_peer("C")}}
  end

  # simple test with two peers
  test "messages are spread across peers", %{
    peer_1: %{
      peer_manager: peer_manager_1,
      api_port: api_port_1,
      api_supervisor: api_supervisor_1
    },
    peer_2: %{
      peer_manager: peer_manager_2,
      p2p_port: p2p_port,
      api_port: api_port_2,
      api_supervisor: api_supervisor_2
    }
  } do
    # connect the peers
    VoidPhone.Gossip.PeerManager.connect(peer_manager_1, {{127, 0, 0, 1}, p2p_port})
    # wait for peers to be connected
    eventually(fn ->
      length(VoidPhone.Gossip.PeerManager.get_peers(peer_manager_2)) == 1
    end)

    # for coverage send some push and pull requests to verify that nothing crashes
    send(peer_manager_1, :push_peers)
    send(peer_manager_2, :push_peers)
    send(peer_manager_1, :pull_peers)
    send(peer_manager_2, :pull_peers)

    assert {:ok, client1} =
             :gen_tcp.connect({127, 0, 0, 1}, api_port_1, [:binary, packet: 0, active: false])

    assert {:ok, client2} =
             :gen_tcp.connect({127, 0, 0, 1}, api_port_2, [:binary, packet: 0, active: false])

    {:ok, {_, port1}} = :inet.sockname(client1)
    {:ok, {_, port2}} = :inet.sockname(client2)

    # for coverage, send a view update request to verify that nothing breaks
    send(peer_manager_1, :update_view)
    send(peer_manager_2, :update_view)

    # send a gossip notify request for data type 42 on both clients
    assert :ok =
             :gen_tcp.send(
               client1,
               VoidPhone.MessageUtils.build_message(501, <<0::size(16), 42::integer-size(16)>>)
             )

    assert :ok =
             :gen_tcp.send(
               client2,
               VoidPhone.MessageUtils.build_message(501, <<0::size(16), 42::integer-size(16)>>)
             )

    # also send a notify request on client 1 for type 1337
    assert :ok =
             :gen_tcp.send(
               client1,
               VoidPhone.MessageUtils.build_message(501, <<0::size(16), 1337::integer-size(16)>>)
             )

    worker1 = get_api_worker(api_supervisor_1, port1)
    worker2 = get_api_worker(api_supervisor_2, port2)

    # we need to wait until the notify request is processed, otherwise the announcement will be discarded
    flush_queues(worker1)
    flush_queues(worker2)

    # send a gossip announcement for data type 42 from client1
    message_1 =
      VoidPhone.MessageUtils.build_message(
        500,
        <<32::integer-size(8), 0::size(8), 42::integer-size(16), "mydata">>
      )

    # send a gossip announcement for data type 1337 from client1
    message_2 =
      VoidPhone.MessageUtils.build_message(
        500,
        <<32::integer-size(8), 0::size(8), 1337::integer-size(16), "mydata">>
      )

    assert :ok = :gen_tcp.send(client1, message_1)
    assert :ok = :gen_tcp.send(client1, message_2)
    flush_queues(worker1)

    # expect to receive a gossip notification on client2
    assert {:ok,
            <<size::integer-size(16), msgtype::integer-size(16),
              message_id::unsigned-integer-size(16), data_type::binary-size(2),
              data::binary()>> = message} = :gen_tcp.recv(client2, byte_size(message_1), 1000)

    assert size == byte_size(message)
    assert msgtype == 502
    # message_id is a random number
    assert message_id >= 0 and message_id <= 65535
    assert data_type == <<42::integer-size(16)>>
    assert data == "mydata"

    # the notification should NOT arrive on the client that sent it
    assert {:error, :timeout} = :gen_tcp.recv(client1, 0, 50)

    # send validation
    message =
      VoidPhone.MessageUtils.build_message(
        503,
        <<message_id::unsigned-integer-size(16), 0::size(15), 1::size(1)>>
      )

    assert :ok = :gen_tcp.send(client2, message)

    # the notification for 1337 should NOT arrive on client 2
    assert {:error, :timeout} = :gen_tcp.recv(client2, 0, 50)

    :gen_tcp.close(client1)
    :gen_tcp.close(client2)
  end

  # test with three peers
  test "message is spread further when validated", %{
    peer_1: %{
      peer_manager: peer_manager_1,
      p2p_port: p2p_port_1,
      api_port: api_port_1,
      api_supervisor: api_supervisor_1
    },
    peer_2: %{
      peer_manager: peer_manager_2,
      p2p_port: p2p_port_2,
      api_port: api_port_2,
      api_supervisor: api_supervisor_2
    },
    peer_3: %{
      peer_manager: peer_manager_3,
      p2p_port: p2p_port_3,
      api_port: api_port_3,
      api_supervisor: api_supervisor_3
    }
  } do
    # connect the peers
    VoidPhone.Gossip.PeerManager.connect(peer_manager_1, {{127, 0, 0, 1}, p2p_port_2})
    VoidPhone.Gossip.PeerManager.connect(peer_manager_2, {{127, 0, 0, 1}, p2p_port_3})
    # wait for peers to be connected
    eventually(fn ->
      assert length(VoidPhone.Gossip.PeerManager.get_peers(peer_manager_1)) == 1
      assert length(VoidPhone.Gossip.PeerManager.get_peers(peer_manager_2)) == 2
      assert length(VoidPhone.Gossip.PeerManager.get_peers(peer_manager_3)) == 1
    end)

    assert {:ok, client1} =
             :gen_tcp.connect({127, 0, 0, 1}, api_port_1, [:binary, packet: 0, active: false])

    assert {:ok, client2} =
             :gen_tcp.connect({127, 0, 0, 1}, api_port_2, [:binary, packet: 0, active: false])

    assert {:ok, client3} =
             :gen_tcp.connect({127, 0, 0, 1}, api_port_3, [:binary, packet: 0, active: false])

    {:ok, {_, port1}} = :inet.sockname(client1)
    {:ok, {_, port2}} = :inet.sockname(client2)
    {:ok, {_, port3}} = :inet.sockname(client3)

    # send a gossip notify request for data type 42 on all clients
    assert :ok =
             :gen_tcp.send(
               client1,
               VoidPhone.MessageUtils.build_message(501, <<0::size(16), 42::integer-size(16)>>)
             )

    assert :ok =
             :gen_tcp.send(
               client2,
               VoidPhone.MessageUtils.build_message(501, <<0::size(16), 42::integer-size(16)>>)
             )

    assert :ok =
             :gen_tcp.send(
               client3,
               VoidPhone.MessageUtils.build_message(501, <<0::size(16), 42::integer-size(16)>>)
             )

    worker1 = get_api_worker(api_supervisor_1, port1)
    worker2 = get_api_worker(api_supervisor_2, port2)
    worker3 = get_api_worker(api_supervisor_3, port3)

    # we need to wait until the notify request is processed, otherwise the announcement will be discarded
    flush_queues(worker1)
    flush_queues(worker2)
    flush_queues(worker3)

    # send a gossip announcement for data type 42 from client1
    notification_message =
      VoidPhone.MessageUtils.build_message(
        500,
        <<32::integer-size(8), 0::size(8), 42::integer-size(16), "mydata">>
      )

    assert :ok = :gen_tcp.send(client1, notification_message)
    flush_queues(worker1)

    # expect to receive a gossip notification on client2
    assert {:ok,
            <<size::integer-size(16), 502::integer-size(16),
              message_id_1::unsigned-integer-size(16), 42::integer-size(16),
              "mydata"::binary()>> = received_message_1} =
             :gen_tcp.recv(client2, byte_size(notification_message), 1000)

    assert size == byte_size(received_message_1)

    # send validation
    validation_message =
      VoidPhone.MessageUtils.build_message(
        503,
        <<message_id_1::unsigned-integer-size(16), 0::size(15), 1::size(1)>>
      )

    assert :ok = :gen_tcp.send(client2, validation_message)

    # expect to receive a gossip notification on client3
    assert {:ok,
            <<size::integer-size(16), 502::integer-size(16),
              message_id_2::unsigned-integer-size(16), 42::integer-size(16),
              "mydata"::binary()>> = received_message_2} =
             :gen_tcp.recv(client3, byte_size(notification_message), 1000)

    assert size == byte_size(received_message_2)

    # now connect peer three and peer 3 to test duplicate messages
    VoidPhone.Gossip.PeerManager.connect(peer_manager_3, {{127, 0, 0, 1}, p2p_port_1})
    # all peers should be connected
    eventually(fn ->
      assert length(VoidPhone.Gossip.PeerManager.get_peers(peer_manager_1)) == 2
      assert length(VoidPhone.Gossip.PeerManager.get_peers(peer_manager_2)) == 2
      assert length(VoidPhone.Gossip.PeerManager.get_peers(peer_manager_3)) == 2
    end)

    # send validation
    validation_message =
      VoidPhone.MessageUtils.build_message(
        503,
        <<message_id_2::unsigned-integer-size(16), 0::size(15), 1::size(1)>>
      )

    assert :ok = :gen_tcp.send(client3, validation_message)

    # should be quiet now, the message should not arrive anywhere else
    assert {:error, :timeout} = :gen_tcp.recv(client1, 0, 50)
    assert {:error, :timeout} = :gen_tcp.recv(client2, 0, 50)
    assert {:error, :timeout} = :gen_tcp.recv(client3, 0, 50)

    :gen_tcp.close(client1)
    :gen_tcp.close(client2)
  end

  # test with three peers and invalid message
  test "message is not spread further when invalid", %{
    peer_1: %{
      peer_manager: peer_manager_1,
      api_port: api_port_1,
      api_supervisor: api_supervisor_1
    },
    peer_2: %{
      peer_manager: peer_manager_2,
      p2p_port: p2p_port_2,
      api_port: api_port_2,
      api_supervisor: api_supervisor_2
    },
    peer_3: %{
      peer_manager: peer_manager_3,
      p2p_port: p2p_port_3,
      api_port: api_port_3,
      api_supervisor: api_supervisor_3
    }
  } do
    # connect the peers
    VoidPhone.Gossip.PeerManager.connect(peer_manager_1, {{127, 0, 0, 1}, p2p_port_2})
    VoidPhone.Gossip.PeerManager.connect(peer_manager_2, {{127, 0, 0, 1}, p2p_port_3})
    # wait for peers to be connected
    eventually(fn ->
      assert length(VoidPhone.Gossip.PeerManager.get_peers(peer_manager_1)) == 1
      assert length(VoidPhone.Gossip.PeerManager.get_peers(peer_manager_2)) == 2
      assert length(VoidPhone.Gossip.PeerManager.get_peers(peer_manager_3)) == 1
    end)

    assert {:ok, client1} =
             :gen_tcp.connect({127, 0, 0, 1}, api_port_1, [:binary, packet: 0, active: false])

    assert {:ok, client2} =
             :gen_tcp.connect({127, 0, 0, 1}, api_port_2, [:binary, packet: 0, active: false])

    assert {:ok, client3} =
             :gen_tcp.connect({127, 0, 0, 1}, api_port_3, [:binary, packet: 0, active: false])

    {:ok, {_, port1}} = :inet.sockname(client1)
    {:ok, {_, port2}} = :inet.sockname(client2)
    {:ok, {_, port3}} = :inet.sockname(client3)

    # send a gossip notify request for data type 42 on all clients
    assert :ok =
             :gen_tcp.send(
               client1,
               VoidPhone.MessageUtils.build_message(501, <<0::size(16), 42::integer-size(16)>>)
             )

    assert :ok =
             :gen_tcp.send(
               client2,
               VoidPhone.MessageUtils.build_message(501, <<0::size(16), 42::integer-size(16)>>)
             )

    assert :ok =
             :gen_tcp.send(
               client3,
               VoidPhone.MessageUtils.build_message(501, <<0::size(16), 42::integer-size(16)>>)
             )

    worker1 = get_api_worker(api_supervisor_1, port1)
    worker2 = get_api_worker(api_supervisor_2, port2)
    worker3 = get_api_worker(api_supervisor_3, port3)

    # we need to wait until the notify request is processed, otherwise the announcement will be discarded
    flush_queues(worker1)
    flush_queues(worker2)
    flush_queues(worker3)

    # send a gossip announcement for data type 42 from client1
    notification_message =
      VoidPhone.MessageUtils.build_message(
        500,
        <<32::integer-size(8), 0::size(8), 42::integer-size(16), "mydata">>
      )

    assert :ok = :gen_tcp.send(client1, notification_message)
    flush_queues(worker1)

    # expect to receive a gossip notification on client2
    assert {:ok,
            <<size::integer-size(16), 502::integer-size(16),
              message_id_1::unsigned-integer-size(16), 42::integer-size(16),
              "mydata"::binary()>> = received_message_1} =
             :gen_tcp.recv(client2, byte_size(notification_message), 1000)

    assert size == byte_size(received_message_1)

    # send (in)validation
    validation_message =
      VoidPhone.MessageUtils.build_message(
        503,
        <<message_id_1::unsigned-integer-size(16), 0::size(15), 0::size(1)>>
      )

    assert :ok = :gen_tcp.send(client2, validation_message)

    # should be quiet now, the message should not arrive anywhere else
    assert {:error, :timeout} = :gen_tcp.recv(client1, 0, 50)
    assert {:error, :timeout} = :gen_tcp.recv(client2, 0, 50)
    assert {:error, :timeout} = :gen_tcp.recv(client3, 0, 50)

    :gen_tcp.close(client1)
    :gen_tcp.close(client2)
  end
end
