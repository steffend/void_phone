defmodule VoidPhone.Gossip.P2pIntegrationTest do
  use ExUnit.Case, async: true

  import Liveness

  @peer_manager Module.concat(__MODULE__, PeerManager)
  @p2p_supervisor Module.concat(__MODULE__, P2pSupervisor)
  @notification_registry Module.concat(__MODULE__, NotificationRegistry)
  @knowledge_base Module.concat(__MODULE__, KnowledgeBase)

  setup do
    # setup all the required processes and servers
    # to run isolated from other tests
    # by using ephemeral ports for all servers
    {:ok, _} =
      start_supervised({DynamicSupervisor, strategy: :one_for_one, name: @p2p_supervisor})

    {:ok, _} = start_supervised({Registry, keys: :duplicate, name: @notification_registry})

    {:ok, socket} = :gen_tcp.listen(0, [:binary, packet: 2, active: :once, reuseaddr: true])
    {:ok, {_, p2p_port}} = :inet.sockname(socket)

    {:ok, _} =
      start_supervised(
        Supervisor.child_spec({Cachex, name: @knowledge_base, limit: 32},
          id: @knowledge_base
        )
      )

    {:ok, _} =
      start_supervised(
        {VoidPhone.Gossip.P2p,
         listen_socket: socket,
         listen_address: "127.0.0.1:#{p2p_port}",
         challenge_difficulty: 1,
         challenge_timeout: 200,
         challenge_recv_timeout: 200,
         supervisor: @p2p_supervisor,
         api_registry: @notification_registry,
         peer_manager: @peer_manager,
         knowledge_base: @knowledge_base}
      )

    {:ok, _} =
      start_supervised(
        {VoidPhone.Gossip.PeerManager,
         name: @peer_manager,
         bootstrapper: nil,
         address: "127.0.0.1:#{p2p_port}",
         max_peers: 3,
         push_interval: 600,
         pull_interval: 600,
         update_interval: 1200,
         sampler_check_interval: 600}
      )

    {:ok, %{p2p_port: p2p_port, challenge_recv_timeout: 200}}
  end

  defp get_p2p_worker(port) do
    eventually(fn ->
      DynamicSupervisor.which_children(@p2p_supervisor)
      |> then(fn children ->
        assert length(children) > 0
        children
      end)
      |> Enum.map(fn {_, pid, _, _} -> pid end)
      |> Enum.find(fn pid ->
        match?(%{peer_address: {{127, 0, 0, 1}, ^port}}, :sys.get_state(pid))
      end)
      |> then(fn pid ->
        assert is_pid(pid)
        pid
      end)
    end)
  end

  defp synchronize_p2p(socket) do
    assert :ok = :gen_tcp.send(socket, :erlang.term_to_binary(:ping))
    assert {:ok, :erlang.term_to_binary(:pong)} == :gen_tcp.recv(socket, 0, 500)
  end

  defp setup_p2p(%{p2p_port: p2p_port}) do
    assert {:ok, client} =
             :gen_tcp.connect({127, 0, 0, 1}, p2p_port, [
               :binary,
               packet: 2,
               active: false
             ])

    {:ok, {_ip, port}} = :inet.sockname(client)

    assert {:ok, data} = :gen_tcp.recv(client, 0, 500)
    assert {:challenge_request, difficulty, data} = :erlang.binary_to_term(data)

    {:ok, nonce} = VoidPhone.Gossip.P2pUtils.pow(difficulty, data, :infinity)

    assert :ok = :gen_tcp.send(client, :erlang.term_to_binary({:challenge_response, data, nonce}))

    # announce address
    assert :ok =
             :gen_tcp.send(
               client,
               :erlang.term_to_binary({:p2p_address, {{127, 0, 0, 1}, port}})
             )

    assert synchronize_p2p(client)
    worker = get_p2p_worker(port)

    {:ok, client, worker}
  end

  describe "p2p" do
    test "connects to p2p server and receives challenge request", %{p2p_port: p2p_port} do
      assert {:ok, client} =
               :gen_tcp.connect({127, 0, 0, 1}, p2p_port, [
                 :binary,
                 packet: 2,
                 active: false
               ])

      assert {:ok, data} = :gen_tcp.recv(client, 0, 500)
      assert {:challenge_request, difficulty, data} = :erlang.binary_to_term(data)
      # test config is set in the test/test_helper.exs
      assert difficulty == 1
      # we are using random 16 byte data
      assert byte_size(data) == 16
    end

    test "server closes connection when challenge is wrong", %{p2p_port: p2p_port} do
      assert {:ok, client} =
               :gen_tcp.connect({127, 0, 0, 1}, p2p_port, [
                 :binary,
                 packet: 2,
                 active: false
               ])

      assert {:ok, data} = :gen_tcp.recv(client, 0, 500)
      assert {:challenge_request, difficulty, data} = :erlang.binary_to_term(data)
      # test config is set in the test/test_helper.exs
      assert difficulty == 1
      # we are using random 16 byte data
      assert byte_size(data) == 16

      # create an invalid nonce that does not by chance validates the challenge
      invalid_nonce =
        eventually(fn ->
          nonce = :crypto.strong_rand_bytes(8)
          msg = <<data::binary(), nonce::binary()>>
          hash = :crypto.hash(:sha256, msg)

          case hash do
            <<0::size(difficulty)-unit(8), _rest::binary()>> ->
              # this actually matches, bad...
              raise "fail"

            _other ->
              nonce
          end
        end)

      assert :ok =
               :gen_tcp.send(
                 client,
                 :erlang.term_to_binary({:challenge_response, data, invalid_nonce})
               )

      assert {:error, :closed} = :gen_tcp.recv(client, 0, 1000)
    end

    test "server does not close connection when challenge is correct", opts do
      # the setup helper connects and does the challenge for us
      assert {:ok, _client, _worker} = setup_p2p(opts)
    end

    test "closes the connection when not receiving a challenge response", %{
      p2p_port: p2p_port,
      challenge_recv_timeout: timeout
    } do
      assert {:ok, client} =
               :gen_tcp.connect({127, 0, 0, 1}, p2p_port, [
                 :binary,
                 packet: 2,
                 active: false
               ])

      assert {:ok, data} = :gen_tcp.recv(client, 0, 500)
      assert {:challenge_request, difficulty, data} = :erlang.binary_to_term(data)
      # test config is set in the test/test_helper.exs
      assert difficulty == 1
      # we are using random 16 byte data
      assert byte_size(data) == 16

      Process.sleep(timeout)

      assert {:error, :closed} = :gen_tcp.recv(client, 0, 500)
    end

    test "sends push_request and pull_request messages", opts = %{p2p_port: p2p_port} do
      assert {:ok, client, worker} = setup_p2p(opts)
      # tell the worker to send a push request
      send(worker, :push_request)
      assert {:ok, data} = :gen_tcp.recv(client, 0, 500)
      assert :push_request = :erlang.binary_to_term(data)

      # tell the worker to send a pull request
      send(worker, :pull_request)
      assert {:ok, data} = :gen_tcp.recv(client, 0, 500)
      assert :pull_request = :erlang.binary_to_term(data)

      # answer with a fake peer
      assert :ok =
               :gen_tcp.send(
                 client,
                 :erlang.term_to_binary({:pull_response,
                  [
                    {{127, 0, 0, 1}, 1234},
                    # the tested peer itself
                    {{127, 0, 0, 1}, p2p_port}
                  ]})
               )

      assert synchronize_p2p(client)
    end

    test "closes the connection when receiving an unexpected pull_response", opts do
      assert {:ok, client, _worker} = setup_p2p(opts)

      assert :ok = :gen_tcp.send(client, :erlang.term_to_binary({:pull_response, []}))

      assert {:error, :closed} = :gen_tcp.recv(client, 0, 500)
    end

    test "replies to ping", opts do
      assert {:ok, client, worker} = setup_p2p(opts)

      task =
        Task.async(fn ->
          assert :pong = VoidPhone.Gossip.P2pWorker.ping(worker)
        end)

      assert {:ok, data} = :gen_tcp.recv(client, 0, 500)
      assert :ping = :erlang.binary_to_term(data)
      assert :ok = :gen_tcp.send(client, :erlang.term_to_binary(:pong))
      Task.await(task)
    end

    test "fails when receiving pong without ping", opts do
      assert {:ok, client, worker} = setup_p2p(opts)
      ref = Process.monitor(worker)
      assert :ok = :gen_tcp.send(client, :erlang.term_to_binary(:pong))
      assert_receive {:DOWN, ^ref, :process, ^worker, :abnormal}
    end

    test "fails when receiving invalid data", opts do
      assert {:ok, client, worker} = setup_p2p(opts)
      ref = Process.monitor(worker)
      assert :ok = :gen_tcp.send(client, <<1, 2, 3, 4>>)
      assert_receive {:DOWN, ^ref, :process, ^worker, :invalid_message}
    end

    test "fails when receiving unknown message", opts do
      assert {:ok, client, worker} = setup_p2p(opts)
      ref = Process.monitor(worker)
      assert :ok = :gen_tcp.send(client, :erlang.term_to_binary(:woops_what_is_going_on))
      assert_receive {:DOWN, ^ref, :process, ^worker, :abnormal}
    end
  end
end
