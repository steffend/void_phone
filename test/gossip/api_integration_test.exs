defmodule VoidPhone.Gossip.ApiIntegrationTest do
  use ExUnit.Case, async: true
  import Liveness

  @peer_manager Module.concat(__MODULE__, PeerManager)
  @api_supervisor Module.concat(__MODULE__, ApiSupervisor)
  @notification_registry Module.concat(__MODULE__, NotificationRegistry)
  @knowledge_base Module.concat(__MODULE__, KnowledgeBase)
  @validation_cache Module.concat(__MODULE__, ValidationCache)

  setup do
    # setup all the required processes and servers
    # to run isolated from other tests
    # by using ephemeral ports for all servers
    {:ok, _} =
      start_supervised(
        Supervisor.child_spec({Cachex, name: @knowledge_base, limit: 32},
          id: @knowledge_base
        )
      )

    {:ok, _} =
      start_supervised(
        Supervisor.child_spec(
          {Cachex, name: @validation_cache, limit: 32},
          id: @validation_cache
        )
      )

    {:ok, _} =
      start_supervised({DynamicSupervisor, strategy: :one_for_one, name: @api_supervisor})

    {:ok, _} = start_supervised({Registry, keys: :duplicate, name: @notification_registry})

    {:ok, socket} = :gen_tcp.listen(0, [:binary, packet: :raw, active: false, reuseaddr: true])
    {:ok, {_, api_port}} = :inet.sockname(socket)

    {:ok, _} =
      start_supervised(
        {VoidPhone.Gossip.Api,
         listen_socket: socket,
         listen_address: "127.0.0.1:#{api_port}",
         supervisor: @api_supervisor,
         registry: @notification_registry,
         peer_manager: @peer_manager,
         knowledge_base: @knowledge_base,
         validation_cache: @validation_cache}
      )

    {:ok, _} =
      start_supervised({
        VoidPhone.Gossip.PeerManager,
        # not really used, so the port does not matter
        name: @peer_manager,
        bootstrapper: nil,
        address: "127.0.0.1:1234",
        max_peers: 3,
        push_interval: 600,
        pull_interval: 600,
        update_interval: 1200,
        sampler_check_interval: 600
      })

    {:ok, %{api_port: api_port}}
  end

  defp get_api_worker(port) do
    eventually(fn ->
      DynamicSupervisor.which_children(@api_supervisor)
      |> then(fn children ->
        assert length(children) > 0
        children
      end)
      |> Enum.map(fn {_, pid, _, _} -> pid end)
      |> Enum.find(fn pid ->
        match?(%{client_address: {{127, 0, 0, 1}, ^port}}, :sys.get_state(pid))
      end)
      |> then(fn pid ->
        assert is_pid(pid)
        pid
      end)
    end)
  end

  defp flush_queues(worker) do
    eventually(fn ->
      assert {_, 0} = :erlang.process_info(worker, :message_queue_len)
    end)
  end

  describe "api" do
    test "connects to api server", %{api_port: api_port} do
      # connect two clients locally
      assert {:ok, client1} =
               :gen_tcp.connect({127, 0, 0, 1}, api_port, [:binary, packet: 0, active: false])

      assert {:ok, client2} =
               :gen_tcp.connect({127, 0, 0, 1}, api_port, [:binary, packet: 0, active: false])

      # two clients should be connected
      eventually(fn ->
        assert length(DynamicSupervisor.which_children(@api_supervisor)) == 2
      end)

      :gen_tcp.close(client1)
      :gen_tcp.close(client2)
    end

    test "forwards messages locally if there is a subscriber", %{api_port: api_port} do
      assert {:ok, client1} =
               :gen_tcp.connect({127, 0, 0, 1}, api_port, [:binary, packet: 0, active: false])

      assert {:ok, client2} =
               :gen_tcp.connect({127, 0, 0, 1}, api_port, [:binary, packet: 0, active: false])

      {:ok, {_, port1}} = :inet.sockname(client1)
      {:ok, {_, port2}} = :inet.sockname(client2)

      # send a gossip notify request for data type 42 on both clients
      assert :ok =
               :gen_tcp.send(
                 client1,
                 VoidPhone.MessageUtils.build_message(501, <<0::size(16), 42::integer-size(16)>>)
               )

      assert :ok =
               :gen_tcp.send(
                 client2,
                 VoidPhone.MessageUtils.build_message(501, <<0::size(16), 42::integer-size(16)>>)
               )

      worker1 = get_api_worker(port1)
      worker2 = get_api_worker(port2)

      # we need to wait until the notify request is processed, otherwise the announcement will be discarded
      flush_queues(worker1)
      flush_queues(worker2)

      # send a gossip announcement for data type 42 from client1
      message =
        VoidPhone.MessageUtils.build_message(
          500,
          <<32::integer-size(8), 0::size(8), 42::integer-size(16), "mydata">>
        )

      assert :ok = :gen_tcp.send(client1, message)
      flush_queues(worker1)

      # expect to receive a gossip notification on client2
      assert {:ok,
              <<size::integer-size(16), msgtype::integer-size(16),
                message_id::unsigned-integer-size(16), data_type::binary-size(2),
                data::binary()>> = message} = :gen_tcp.recv(client2, byte_size(message), 1000)

      assert size == byte_size(message)
      assert msgtype == 502
      # message_id is a random number
      assert message_id >= 0 and message_id <= 65535
      assert data_type == <<42::integer-size(16)>>
      assert data == "mydata"

      # the notification should NOT arrive on the client that sent it
      assert {:error, :timeout} = :gen_tcp.recv(client1, 0, 50)

      # send validation
      message =
        VoidPhone.MessageUtils.build_message(
          503,
          <<message_id::unsigned-integer-size(16), 0::size(15), 1::size(1)>>
        )

      assert :ok = :gen_tcp.send(client2, message)

      assert {:error, :timeout} = :gen_tcp.recv(client2, 0, 50)

      :gen_tcp.close(client1)
      :gen_tcp.close(client2)
    end

    test "does not forward message if there is no subscriber", %{api_port: api_port} do
      assert {:ok, client1} =
               :gen_tcp.connect({127, 0, 0, 1}, api_port, [:binary, packet: 0, active: false])

      assert {:ok, client2} =
               :gen_tcp.connect({127, 0, 0, 1}, api_port, [:binary, packet: 0, active: false])

      {:ok, {_, port1}} = :inet.sockname(client1)
      # send a gossip notify request for data type 42, but only on client1
      assert :ok =
               :gen_tcp.send(
                 client1,
                 VoidPhone.MessageUtils.build_message(501, <<0::size(16), 42::integer-size(16)>>)
               )

      worker1 = get_api_worker(port1)

      # we need to wait until the notify request is processed, otherwise the announcement will be discarded
      flush_queues(worker1)

      # send a gossip announcement for data type 42 from client1
      message =
        VoidPhone.MessageUtils.build_message(
          500,
          <<32::integer-size(8), 0::size(8), 42::integer-size(16), "mydata">>
        )

      assert :ok = :gen_tcp.send(client1, message)
      flush_queues(worker1)

      assert {:error, :timeout} = :gen_tcp.recv(client1, 0, 50)
      assert {:error, :timeout} = :gen_tcp.recv(client2, 0, 50)

      :gen_tcp.close(client1)
      :gen_tcp.close(client2)
    end

    test "fails when receiving invalid data", %{api_port: api_port} do
      # connect two clients locally
      assert {:ok, client} =
               :gen_tcp.connect({127, 0, 0, 1}, api_port, [:binary, packet: 0, active: false])

      {:ok, {_, port}} = :inet.sockname(client)
      worker = get_api_worker(port)
      ref = Process.monitor(worker)
      # 666 is not a valid gossip message id
      assert :ok = :gen_tcp.send(client, VoidPhone.MessageUtils.build_message(666, <<>>))

      assert_receive {:DOWN, ^ref, :process, ^worker, :abnormal}
    end
  end
end
