defmodule TestTcpServer do
  use VoidPhone.Gossip.TcpServer,
    supervisor: TcpServerTestSupervisor,
    worker: TcpServerTestWorker,
    listen_opts: [:binary, packet: :line, active: true, reuseaddr: true],
    type: "test"
end

defmodule TcpServerTestWorker do
  use GenServer

  def init(socket), do: {:ok, socket}

  def handle_info({:tcp, socket, data}, state) do
    # echo!
    :gen_tcp.send(socket, data)
    {:noreply, state}
  end

  def handle_info({:tcp_closed, _socket}, state) do
    {:stop, :normal, state}
  end

  def handle_info({:tcp_error, _socket, reason}, state) do
    {:stop, reason, state}
  end

  def start_link(socket), do: GenServer.start_link(__MODULE__, socket, [])
end

defmodule VoidPhone.Gossip.TcpServerTest do
  use ExUnit.Case, async: true

  test "starts a tcp server that listens on the specified port" do
    start_supervised({DynamicSupervisor, strategy: :one_for_one, name: TcpServerTestSupervisor})

    assert {:ok, pid} = TestTcpServer.start_link(listen_address: "127.0.0.1:7005")
    assert is_pid(pid)

    # wait until the socket is open
    Process.sleep(50)

    assert {:ok, socket} =
             :gen_tcp.connect({127, 0, 0, 1}, 7005, [:binary, packet: :line, active: false])

    :gen_tcp.send(socket, "hello\n")
    assert {:ok, "hello\n"} = :gen_tcp.recv(socket, 0)
  end
end
