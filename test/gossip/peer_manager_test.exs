defmodule VoidPhone.Gossip.PeerManagerTest do
  use ExUnit.Case, async: true
  doctest VoidPhone.Gossip.PeerManager

  @peer_manager Module.concat(__MODULE__, PeerManager)
  @peer_registry Module.concat(__MODULE__, PeerRegistry)
  @api_registry Module.concat(__MODULE__, NotificationRegistry)
  @p2p_supervisor Module.concat(__MODULE__, P2pSupervisor)

  alias VoidPhone.Gossip.{PeerManager, P2pUtils}

  setup do
    {:ok, _} =
      start_supervised({DynamicSupervisor, strategy: :one_for_one, name: @p2p_supervisor})

    {:ok, _} = start_supervised({Registry, keys: :unique, name: @peer_registry})
    {:ok, _} = start_supervised({Registry, keys: :duplicate, name: @api_registry})

    seeded_samplers =
      for i <- 1..3 do
        VoidPhone.Gossip.BrahmsSampler.new(<<i>>)
      end

    {:ok, _} =
      start_supervised({
        VoidPhone.Gossip.PeerManager,
        # instantly retry
        name: @peer_manager,
        registry: @peer_registry,
        api_registry: @api_registry,
        supervisor: @p2p_supervisor,
        bootstrapper: nil,
        address: "127.0.0.1:1234",
        max_peers: 3,
        push_interval: 600,
        pull_interval: 600,
        update_interval: 1200,
        sampler_check_interval: 600,
        samplers: seeded_samplers,
        sampler_check_ping_timeout: 50,
        sampler_check_dest: self(),
        seed: 1,
        retry_handler: fn _ -> 0 end
      })

    {:ok, %{}}
  end

  describe "connect" do
    defp test_connect(peer_formatter, difficulty) do
      {:ok, server} =
        :gen_tcp.listen(0, [
          :binary,
          ip: {127, 0, 0, 1},
          packet: 2,
          active: false,
          reuseaddr: true
        ])

      {:ok, {_, port}} = :inet.sockname(server)

      task =
        Task.async(fn ->
          {:ok, socket} = :gen_tcp.accept(server)
          rand_data = :crypto.strong_rand_bytes(16)

          :gen_tcp.send(
            socket,
            :erlang.term_to_binary({:challenge_request, difficulty, rand_data})
          )

          {:ok, msg} = :gen_tcp.recv(socket, 0, 500)
          assert {:challenge_response, ^rand_data, nonce} = :erlang.binary_to_term(msg)
          assert P2pUtils.valid_pow?(difficulty, rand_data, nonce)
          assert :ok = :gen_tcp.close(socket)
        end)

      PeerManager.connect(@peer_manager, peer_formatter.(port))
      Task.await(task)
    end

    test "starts a new p2p worker" do
      # we can either use a string as in the config, or an {ip, port} tuple
      # as used internally in the p2p protocol
      Task.await_many([
        Task.async(fn -> test_connect(fn port -> "127.0.0.1:#{port}" end, 1) end),
        Task.async(fn -> test_connect(fn port -> {{127, 0, 0, 1}, port} end, 0) end)
      ])
    end
  end

  describe "register" do
    test "registers in the peer registry" do
      assert :ok = PeerManager.register(@peer_manager, {{127, 0, 0, 1}, 8888}, @peer_registry)
      assert [{{127, 0, 0, 1}, 8888}] = PeerManager.get_peers(@peer_manager)
      assert [self()] == PeerManager.get_peer_pids(@peer_manager)
    end
  end

  describe "push" do
    test "registers when there is still enough space" do
      assert :ok = PeerManager.push(@peer_manager, {{127, 0, 0, 1}, 1}, @peer_registry)
      assert :ok = PeerManager.push(@peer_manager, {{127, 0, 0, 1}, 2}, @peer_registry)
      assert :ok = PeerManager.push(@peer_manager, {{127, 0, 0, 1}, 3}, @peer_registry)
      # the fourth peer is treated as a push request
      assert :ok = PeerManager.push(@peer_manager, {{127, 0, 0, 1}, 4}, @peer_registry)

      assert [{{127, 0, 0, 1}, 1}, {{127, 0, 0, 1}, 2}, {{127, 0, 0, 1}, 3}] =
               PeerManager.get_peers(@peer_manager) |> Enum.sort()

      assert [self(), self(), self()] == PeerManager.get_peer_pids(@peer_manager)

      assert %{push_peers: push_peers} = :sys.get_state(@peer_manager)
      assert [{{127, 0, 0, 1}, 4}] = MapSet.to_list(push_peers)
    end

    test "treats already registered peers as push requests" do
      assert :ok = PeerManager.push(@peer_manager, {{127, 0, 0, 1}, 1}, @peer_registry)
      # now push again
      assert :ok = PeerManager.push(@peer_manager, {{127, 0, 0, 1}, 1}, @peer_registry)

      assert [{{127, 0, 0, 1}, 1}] = PeerManager.get_peers(@peer_manager)
      assert [self()] == PeerManager.get_peer_pids(@peer_manager)

      assert %{push_peers: push_peers} = :sys.get_state(@peer_manager)
      assert [{{127, 0, 0, 1}, 1}] = MapSet.to_list(push_peers)
    end
  end

  describe "pull_response" do
    test "merges the specified peers into the pull set" do
      # simulate two received pull responses
      assert :ok =
               PeerManager.pull_response(@peer_manager, [{{127, 0, 0, 1}, 1}, {{127, 0, 0, 1}, 2}])

      assert :ok =
               PeerManager.pull_response(@peer_manager, [{{127, 0, 0, 1}, 1}, {{127, 0, 0, 1}, 3}])

      assert %{pull_peers: pull_peers} = :sys.get_state(@peer_manager)

      assert [{{127, 0, 0, 1}, 1}, {{127, 0, 0, 1}, 2}, {{127, 0, 0, 1}, 3}] =
               MapSet.to_list(pull_peers) |> Enum.sort()
    end
  end

  describe "bootstrap" do
    test "connects to bootstrapper and reconnects" do
      {:ok, server} =
        :gen_tcp.listen(0, [
          :binary,
          ip: {127, 0, 0, 1},
          packet: 2,
          active: false,
          reuseaddr: true
        ])

      {:ok, {_, p2p_port}} = :inet.sockname(server)

      bootstrap_task =
        Task.async(fn ->
          assert {:ok, socket} = :gen_tcp.accept(server)
          rand_data = :crypto.strong_rand_bytes(16)
          :gen_tcp.send(socket, :erlang.term_to_binary({:challenge_request, 1, rand_data}))
          {:ok, msg} = :gen_tcp.recv(socket, 0, 500)
          assert {:challenge_response, ^rand_data, nonce} = :erlang.binary_to_term(msg)
          assert P2pUtils.valid_pow?(1, rand_data, nonce)
          # sends the p2p address to us
          {:ok, msg} = :gen_tcp.recv(socket, 0, 500)
          assert {:p2p_address, {{127, 0, 0, 1}, 1234}} = :erlang.binary_to_term(msg)
          # now simulate the bootstrapper dying
          # the client should reconnect
          assert :ok = :gen_tcp.close(socket)
          assert {:ok, socket} = :gen_tcp.accept(server)
          :gen_tcp.send(socket, :erlang.term_to_binary({:challenge_request, 0, <<>>}))
          {:ok, msg} = :gen_tcp.recv(socket, 0, 500)
          assert {:challenge_response, _, _} = :erlang.binary_to_term(msg)
        end)

      # create a separate peer manager for this test
      # with the specified bootstrapper
      assert {:ok, _} =
               start_supervised(
                 Supervisor.child_spec(
                   {VoidPhone.Gossip.PeerManager,
                    name: Module.concat(@peer_manager, TestBootstrap),
                    registry: @peer_registry,
                    api_registry: @api_registry,
                    supervisor: @p2p_supervisor,
                    bootstrapper: "127.0.0.1:#{p2p_port}",
                    address: "127.0.0.1:1234"},
                   id: Module.concat(@peer_manager, TestBootstrap)
                 )
               )

      Task.await(bootstrap_task)
    end
  end

  describe "brahms" do
    test "assembles new view out of push, pull and history samples" do
      # this test uses the fact that we seeded the random operations in the setup block,
      # otherwise it would not be deterministic
      assert :ok = PeerManager.register(@peer_manager, {{127, 0, 0, 1}, 1}, @peer_registry)
      assert :ok = PeerManager.register(@peer_manager, {{127, 0, 0, 1}, 2}, @peer_registry)
      pid = self()
      # the third peer is spawned in the background, as the task will be killed
      # when the new peer is selected for the view
      # and we do not want that the manager kills our test process...
      to_be_killed =
        spawn(fn ->
          PeerManager.register(@peer_manager, {{127, 0, 0, 1}, 3}, @peer_registry)
          send(pid, :registered)
          Process.sleep(:infinity)
        end)

      # sync
      assert_receive :registered

      # simulate push and pull requests / responses
      assert :ok = PeerManager.push(@peer_manager, {{127, 0, 0, 1}, 4}, @peer_registry)

      assert :ok =
               PeerManager.pull_response(@peer_manager, [{{127, 0, 0, 1}, 1}, {{127, 0, 0, 1}, 2}])

      assert :ok =
               PeerManager.pull_response(@peer_manager, [
                 {{127, 0, 0, 1}, 2},
                 {{127, 0, 0, 1}, 4},
                 {{127, 0, 0, 1}, 3}
               ])

      # now inspect the state
      assert %{peers: peers, push_peers: push_peers, pull_peers: pull_peers, samplers: samplers} =
               :sys.get_state(@peer_manager)

      assert [{{127, 0, 0, 1}, 1}, {{127, 0, 0, 1}, 2}, {{127, 0, 0, 1}, 3}] =
               MapSet.to_list(peers) |> Enum.sort()

      sampled_peers =
        Enum.map(samplers, fn sampler -> VoidPhone.Gossip.BrahmsSampler.sample(sampler) end)
        |> Enum.uniq()

      # at least one peer must be in the sampler
      assert length(sampled_peers) > 0

      # compare push and pull peers with the expected values
      assert [{{127, 0, 0, 1}, 1}, {{127, 0, 0, 1}, 2}, {{127, 0, 0, 1}, 3}, {{127, 0, 0, 1}, 4}] =
               MapSet.to_list(pull_peers) |> Enum.sort()

      assert [{{127, 0, 0, 1}, 4}] = MapSet.to_list(push_peers) |> Enum.to_list() |> Enum.sort()

      # now trigger the view update
      send(@peer_manager, :update_view)

      assert %{peers: peers} = :sys.get_state(@peer_manager)

      # peer 3 was replaced by peer 4
      assert MapSet.equal?(
               MapSet.new([
                 {{127, 0, 0, 1}, 1},
                 {{127, 0, 0, 1}, 2},
                 {{127, 0, 0, 1}, 4}
               ]),
               peers
             )

      # the connection to peer 3 was closed
      refute Process.alive?(to_be_killed)
    end

    test "periodically sends push requests to random peers" do
      # only alpha * l1 requests receive a push request every round
      pid = self()

      # simulate three tasks, only two of them receive a push request
      # as we randomly choose them from the list of peers
      lucky_task =
        Task.async(fn ->
          PeerManager.register(@peer_manager, {{127, 0, 0, 1}, 3}, @peer_registry)
          send(pid, :registered_1)
          assert_receive :push_request, 500
        end)

      second_lucky_task =
        Task.async(fn ->
          PeerManager.register(@peer_manager, {{127, 0, 0, 1}, 2}, @peer_registry)
          send(pid, :registered_2)
          assert_receive :push_request, 500
        end)

      unlucky_task =
        Task.async(fn ->
          PeerManager.register(@peer_manager, {{127, 0, 0, 1}, 1}, @peer_registry)
          send(pid, :registered_3)
          refute_receive :push_request, 500
        end)

      # synchronize with the tasks
      assert_receive :registered_1
      assert_receive :registered_2
      assert_receive :registered_3

      send(@peer_manager, :push_peers)

      Task.await_many([lucky_task, second_lucky_task, unlucky_task])
    end

    test "periodically sends pull requests to random peers" do
      # only alpha * l1 requests receive a push request every round
      pid = self()

      # simulate three tasks, only two of them receive a pull request
      # as we randomly choose them from the list of peers
      lucky_task =
        Task.async(fn ->
          PeerManager.register(@peer_manager, {{127, 0, 0, 1}, 3}, @peer_registry)
          send(pid, :registered_1)
          assert_receive :pull_request, 500
        end)

      second_lucky_task =
        Task.async(fn ->
          PeerManager.register(@peer_manager, {{127, 0, 0, 1}, 2}, @peer_registry)
          send(pid, :registered_2)
          assert_receive :pull_request, 500
        end)

      unlucky_task =
        Task.async(fn ->
          PeerManager.register(@peer_manager, {{127, 0, 0, 1}, 1}, @peer_registry)
          send(pid, :registered_3)
          refute_receive :pull_request, 500
        end)

      # synchronize with the tasks
      assert_receive :registered_1
      assert_receive :registered_2
      assert_receive :registered_3

      send(@peer_manager, :pull_peers)

      Task.await_many([lucky_task, second_lucky_task, unlucky_task])
    end

    test "removes dead peers from sampler" do
      # register a few peers; only the first one is alive
      assert {:ok, alive_sock} = :gen_tcp.listen(0, [:binary, packet: 2, active: false])
      assert {:ok, timeout_sock} = :gen_tcp.listen(0, [:binary, packet: 2, active: false])
      assert {:ok, dead_sock} = :gen_tcp.listen(0, [:binary, packet: 2, active: false])
      # get the ephemeral ports
      assert {:ok, {_, alive_port}} = :inet.sockname(alive_sock)
      assert {:ok, {_, timeout_port}} = :inet.sockname(timeout_sock)
      assert {:ok, {_, dead_port}} = :inet.sockname(dead_sock)

      alive_task =
        Task.async(fn ->
          assert {:ok, socket} = :gen_tcp.accept(alive_sock)
          {:ok, msg} = :gen_tcp.recv(socket, 0, 500)
          assert :ping = :erlang.binary_to_term(msg)
          assert :ok = :gen_tcp.send(socket, :erlang.term_to_binary(:pong))
          assert :ok = :gen_tcp.close(socket)
        end)

      timeout_task =
        Task.async(fn ->
          assert {:ok, _socket} = :gen_tcp.accept(timeout_sock)
          Process.sleep(100)
        end)

      dead_task =
        Task.async(fn ->
          assert {:ok, socket} = :gen_tcp.accept(dead_sock)
          assert :ok = :gen_tcp.close(socket)
        end)

      # put all three peers in the sampler
      :sys.replace_state(@peer_manager, fn state ->
        Map.put(state, :samplers, [
          VoidPhone.Gossip.BrahmsSampler.new()
          |> VoidPhone.Gossip.BrahmsSampler.next({{127, 0, 0, 1}, alive_port}),
          VoidPhone.Gossip.BrahmsSampler.new()
          |> VoidPhone.Gossip.BrahmsSampler.next({{127, 0, 0, 1}, timeout_port}),
          VoidPhone.Gossip.BrahmsSampler.new()
          |> VoidPhone.Gossip.BrahmsSampler.next({{127, 0, 0, 1}, dead_port})
        ])
      end)

      # now start the check routine
      send(@peer_manager, :check_sampler)

      # synchronize with the tasks
      Task.await_many([alive_task, timeout_task, dead_task])

      # we hacked ourselves in the sampler_check_dest, therefore
      # we receive the update message here and relay it to the manager
      assert_receive {:update_samplers, samplers}
      send(@peer_manager, {:update_samplers, samplers})

      %{samplers: samplers} = :sys.get_state(@peer_manager)

      # only the alive task should remain in the samplers
      assert Enum.map(samplers, fn sampler -> VoidPhone.Gossip.BrahmsSampler.sample(sampler) end) ==
               [
                 {{127, 0, 0, 1}, alive_port},
                 nil,
                 nil
               ]
    end

    test "tries to connect to new peers" do
      # create socket that will be connected to
      assert {:ok, new_sock} = :gen_tcp.listen(0, [:binary, packet: 2, active: false])
      assert {:ok, {_, new_port}} = :inet.sockname(new_sock)

      # create handler that will reply to the connection request
      new_task =
        Task.async(fn ->
          {:ok, socket} = :gen_tcp.accept(new_sock)
          rand_data = :crypto.strong_rand_bytes(16)

          :gen_tcp.send(
            socket,
            :erlang.term_to_binary({:challenge_request, 1, rand_data})
          )

          {:ok, msg} = :gen_tcp.recv(socket, 0, 500)
          assert {:challenge_response, ^rand_data, nonce} = :erlang.binary_to_term(msg)
          assert P2pUtils.valid_pow?(1, rand_data, nonce)
          assert :ok = :gen_tcp.close(socket)
        end)

      # register three peers (max_peers is 3)
      assert :ok = PeerManager.register(@peer_manager, {{127, 0, 0, 1}, 1}, @peer_registry)
      assert :ok = PeerManager.register(@peer_manager, {{127, 0, 0, 1}, 2}, @peer_registry)
      # the third peer is spawned in the background, as the task will be killed
      # when the new peer is selected for the view
      # and we do not want that the manager kills our test process...
      pid = self()

      spawn(fn ->
        PeerManager.register(@peer_manager, {{127, 0, 0, 1}, 3}, @peer_registry)
        send(pid, :registered)
        Process.sleep(:infinity)
      end)

      # synchronize with the task
      assert_receive :registered

      # there should be at least one pushed peer
      assert :ok = PeerManager.push(@peer_manager, {{127, 0, 0, 1}, 1}, @peer_registry)

      # now construct a new pull view
      assert :ok =
               PeerManager.pull_response(@peer_manager, [{{127, 0, 0, 1}, 1}, {{127, 0, 0, 1}, 2}])

      assert :ok =
               PeerManager.pull_response(@peer_manager, [
                 {{127, 0, 0, 1}, 2},
                 {{127, 0, 0, 1}, new_port},
                 {{127, 0, 0, 1}, 3}
               ])

      # inspect the state to do some assertions
      assert %{peers: peers, push_peers: push_peers, pull_peers: pull_peers} =
               :sys.get_state(@peer_manager)

      # currently we should be connected to 1, 2 and 3
      assert [{{127, 0, 0, 1}, 1}, {{127, 0, 0, 1}, 2}, {{127, 0, 0, 1}, 3}] =
               MapSet.to_list(peers) |> Enum.sort()

      # put 127.0.0.1:4 in all samplers, so that is is chosen to be in the next view
      :sys.replace_state(@peer_manager, fn state ->
        Map.put(state, :samplers, [
          VoidPhone.Gossip.BrahmsSampler.new()
          |> VoidPhone.Gossip.BrahmsSampler.next({{127, 0, 0, 1}, new_port}),
          VoidPhone.Gossip.BrahmsSampler.new()
          |> VoidPhone.Gossip.BrahmsSampler.next({{127, 0, 0, 1}, new_port}),
          VoidPhone.Gossip.BrahmsSampler.new()
          |> VoidPhone.Gossip.BrahmsSampler.next({{127, 0, 0, 1}, new_port})
        ])
      end)

      assert [
               {{127, 0, 0, 1}, 1},
               {{127, 0, 0, 1}, 2},
               {{127, 0, 0, 1}, 3},
               {{127, 0, 0, 1}, ^new_port}
             ] = MapSet.to_list(pull_peers) |> Enum.sort()

      assert [{{127, 0, 0, 1}, 1}] = MapSet.to_list(push_peers) |> Enum.to_list() |> Enum.sort()

      # now trigger the view update
      send(@peer_manager, :update_view)

      assert %{peers: peers} = :sys.get_state(@peer_manager)

      # the peer will only be in the view once it is connected
      assert MapSet.equal?(
               MapSet.new([
                 {{127, 0, 0, 1}, 1},
                 {{127, 0, 0, 1}, 2}
               ]),
               peers
             )

      # wait for the peer manager to connect to the peer
      Task.await(new_task)
    end

    test "removes peer when try_connect fails" do
      # create socket that will be connected to
      assert {:ok, new_sock} = :gen_tcp.listen(0, [:binary, packet: 2, active: false])
      assert {:ok, {_, new_port}} = :inet.sockname(new_sock)

      # create handler that will reply to the connection request
      pid = self()

      Task.start(fn ->
        {:ok, socket} = :gen_tcp.accept(new_sock)
        rand_data = :crypto.strong_rand_bytes(16)

        :gen_tcp.send(
          socket,
          :erlang.term_to_binary({:challenge_request, 1, rand_data})
        )

        {:ok, msg} = :gen_tcp.recv(socket, 0, 500)
        assert {:challenge_response, ^rand_data, nonce} = :erlang.binary_to_term(msg)
        assert P2pUtils.valid_pow?(1, rand_data, nonce)
        send(pid, :connected)
        Process.sleep(:infinity)
      end)

      # simulate a node being in the view + samplers
      :sys.replace_state(@peer_manager, fn state ->
        state
        |> Map.put(:samplers, [
          VoidPhone.Gossip.BrahmsSampler.new()
          |> VoidPhone.Gossip.BrahmsSampler.next({{127, 0, 0, 1}, new_port}),
          VoidPhone.Gossip.BrahmsSampler.new()
          |> VoidPhone.Gossip.BrahmsSampler.next({{127, 0, 0, 1}, new_port}),
          VoidPhone.Gossip.BrahmsSampler.new()
          |> VoidPhone.Gossip.BrahmsSampler.next({{127, 0, 0, 1}, new_port})
        ])
        |> Map.put(:peers, MapSet.new([{{127, 0, 0, 1}, new_port}]))
        |> Map.put(:pull_peers, MapSet.new([{{127, 0, 0, 1}, new_port}]))
        |> Map.put(:push_peers, MapSet.new([{{127, 0, 0, 1}, new_port}]))
      end)

      # connection failure on try 2, tries to reconnect
      send(@peer_manager, {:peer_connected, {{127, 0, 0, 1}, new_port}, {:error, :woops}, 2})
      assert_receive :connected, 1000
      assert PeerManager.get_peers(@peer_manager) == [{{127, 0, 0, 1}, new_port}]

      # simulate failure
      send(@peer_manager, {:peer_connected, {{127, 0, 0, 1}, new_port}, {:error, :woops}, 3})
      # peer should be removed
      assert PeerManager.get_peers(@peer_manager) == []
    end

    test "skips view update when there are not at least one push and one pull request" do
      # register a peer and simulate a push request
      assert :ok = PeerManager.register(@peer_manager, {{127, 0, 0, 1}, 1}, @peer_registry)
      assert :ok = PeerManager.push(@peer_manager, {{127, 0, 0, 1}, 1}, @peer_registry)
      # the push list should contain the peer
      assert %{push_peers: push_peers} = :sys.get_state(@peer_manager)
      assert MapSet.to_list(push_peers) == [{{127, 0, 0, 1}, 1}]
      # skips update and clears all requests as there are no pull requests
      send(@peer_manager, :update_view)
      assert %{push_peers: push_peers} = :sys.get_state(@peer_manager)
      assert MapSet.to_list(push_peers) == []
    end
  end
end
