defmodule VoidPhone.Gossip.P2pUtilsTest do
  use ExUnit.Case, async: true
  doctest VoidPhone.Gossip.P2pUtils

  describe "async_pow/4" do
    test "calls callback with valid nonce" do
      pid = self()
      VoidPhone.Gossip.P2pUtils.async_pow(1, "hello", 1000, fn result -> send(pid, result) end)
      assert_receive {:ok, nonce}
      assert VoidPhone.Gossip.P2pUtils.valid_pow?(1, "hello", nonce)
    end

    test "calls callback with timeout" do
      pid = self()
      VoidPhone.Gossip.P2pUtils.async_pow(3, "hello", 0, fn result -> send(pid, result) end)
      assert_receive {:error, :challenge_timeout}
    end
  end

  describe "ping/1" do
    test "connects to specified socket and sends a ping message" do
      {:ok, server} =
        :gen_tcp.listen(0, [
          :binary,
          ip: {127, 0, 0, 1},
          packet: 2,
          active: false,
          reuseaddr: true
        ])

      {:ok, {_, port}} = :inet.sockname(server)

      Task.start(fn ->
        {:ok, socket} = :gen_tcp.accept(server)
        {:ok, msg} = :gen_tcp.recv(socket, 0)
        assert :erlang.binary_to_term(msg) == :ping
        :gen_tcp.send(socket, :erlang.term_to_binary(:pong))
      end)

      assert :ok = VoidPhone.Gossip.P2pUtils.ping({{127, 0, 0, 1}, port})
    end

    test "returns error on timeout" do
      {:ok, server} =
        :gen_tcp.listen(0, [
          :binary,
          ip: {127, 0, 0, 1},
          packet: 2,
          active: false,
          reuseaddr: true
        ])

      {:ok, {_, port}} = :inet.sockname(server)

      Task.start(fn ->
        {:ok, socket} = :gen_tcp.accept(server)
        {:ok, msg} = :gen_tcp.recv(socket, 0)
        assert :erlang.binary_to_term(msg) == :ping
        :gen_tcp.send(socket, :erlang.term_to_binary(:other_message))
        Process.sleep(50)
      end)

      assert :error = VoidPhone.Gossip.P2pUtils.ping({{127, 0, 0, 1}, port}, 40)
    end
  end
end
