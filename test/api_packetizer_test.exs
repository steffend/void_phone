defmodule VoidPhone.ApiPacketizerTest do
  use ExUnit.Case, async: true

  setup do
    {:ok, server} = start_supervised({EchoServer, self()})

    port =
      receive do
        {^server, :port, port} ->
          port
      after
        1000 -> raise "timeout"
      end

    {:ok, socket} = :gen_tcp.connect({127, 0, 0, 1}, port, [:binary, active: false])

    {:ok, packetizer} =
      start_supervised(
        {VoidPhone.ApiPacketizer, {socket, target: self(), message_recv_timeout: 50}}
      )

    {:ok, %{socket: socket, packetizer: packetizer}}
  end

  describe "packetizer" do
    test "correctly handles messages that are split over more than one tcp segment", %{
      socket: socket
    } do
      # send a gossip announcement split into multiple tcp messages
      actual_message =
        VoidPhone.MessageUtils.build_message(
          500,
          <<32::integer-size(8), 0::size(8), 42::integer-size(16),
            "my_data_with_very_long_content">>
        )

      <<chunk1::binary-size(1), rest::binary()>> = actual_message
      <<chunk2::binary-size(2), rest::binary()>> = rest
      assert :ok = :gen_tcp.send(socket, chunk1)
      assert :ok = :gen_tcp.send(socket, chunk2)
      assert :ok = :gen_tcp.send(socket, rest)

      assert_receive {:tcp, _socket,
                      <<msgtype::integer-size(16), message_id::unsigned-integer-size(16),
                        data_type::binary-size(2), data::binary()>>}

      assert msgtype == 500
      # message_id is a random number
      assert message_id >= 0 and message_id <= 65535
      assert data_type == <<42::integer-size(16)>>
      assert data == "my_data_with_very_long_content"
    end

    test "correctly handles multiple messages in one tcp segment", %{
      socket: socket,
      packetizer: packetizer
    } do
      # send multiple gossip announcement in one tcp message
      first_message =
        VoidPhone.MessageUtils.build_message(
          500,
          <<32::integer-size(8), 0::size(8), 42::integer-size(16), "my_first_announcement">>
        )

      second_message =
        VoidPhone.MessageUtils.build_message(
          500,
          <<32::integer-size(8), 0::size(8), 42::integer-size(16), "my_second_announcement">>
        )

      <<chunk1::binary-size(1), rest::binary()>> = second_message
      <<chunk2::binary-size(2), rest::binary()>> = rest
      assert :ok = :gen_tcp.send(socket, <<first_message::binary(), chunk1::binary()>>)

      assert_receive {:tcp, _socket,
                      <<500::integer-size(16), 32::integer-size(8), 0::size(8),
                        42::integer-size(16), "my_first_announcement">>}

      send(packetizer, :recv_new)

      assert :ok = :gen_tcp.send(socket, <<chunk2::binary(), rest::binary()>>)

      assert_receive {:tcp, _socket,
                      <<500::integer-size(16), 32::integer-size(8), 0::size(8),
                        42::integer-size(16), "my_second_announcement">>}
    end

    test "timeouts", %{socket: socket} do
      # send header, but do not send the rest of the message
      assert :ok = :gen_tcp.send(socket, <<12::integer-size(16), 123::integer-size(16)>>)

      assert_receive {:tcp_error, _socket, :timeout}
    end
  end
end
