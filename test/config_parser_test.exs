defmodule VoidPhone.ConfigParserTest do
  use ExUnit.Case, async: true
  doctest VoidPhone.ConfigParser

  test "parse file" do
    file = Path.dirname(__ENV__.file) <> "/support/testfile.ini"

    assert %{
             :global => %{"hostkey" => "/hostkey.pem"},
             "gossip" => %{
               "api_address" => "131.159.15.61:7001",
               "bootstrapper" => "p2psec.net.in.tum.de:6001",
               "cache_size" => "50",
               "max_connections" => "30",
               "p2p_address" => "131.159.15.61:6001"
             },
             "onion" => %{"hops" => "2"}
           } == VoidPhone.ConfigParser.parse(file)
  end

  test "ignores invalid lines" do
    file = Path.dirname(__ENV__.file) <> "/support/invalid_testfile.ini"

    assert %{
             :global => %{"a" => "b"},
             "mysection" => %{
               "b" => "c"
             }
           } == VoidPhone.ConfigParser.parse(file)
  end
end
