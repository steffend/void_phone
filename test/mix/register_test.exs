defmodule VoidPhone.RegisterServer do
  use VoidPhone.Gossip.TcpServer,
    supervisor: Mix.Tasks.Register.Test.ApiSupervisor,
    worker: VoidPhone.RegisterWorker,
    type: "registration",
    listen_opts: [:binary, packet: :raw, active: false, reuseaddr: true]
end

defmodule Mix.Tasks.Register.Test do
  use ExUnit.Case, async: false

  import ExUnit.CaptureLog

  @api_supervisor Module.concat(__MODULE__, ApiSupervisor)

  setup do
    {:ok, _} =
      start_supervised({DynamicSupervisor, strategy: :one_for_one, name: @api_supervisor})

    {:ok, socket} = :gen_tcp.listen(0, [:binary, packet: :raw, active: false, reuseaddr: true])
    {:ok, {_, api_port}} = :inet.sockname(socket)

    {:ok, _} =
      start_supervised(
        {VoidPhone.RegisterServer,
         listen_socket: socket,
         listen_address: "127.0.0.1:#{api_port}",
         supervisor: @api_supervisor,
         worker: VoidPhone.RegisterWorker,
         pow_difficulty: 1}
      )

    # we want to capture the log messages
    Logger.put_module_level(Mix.Tasks.Register, :info)

    {:ok, %{api_port: api_port}}
  end

  describe "run/1" do
    test "fails when arguments are missing" do
      assert_raise KeyError, fn -> capture_log(fn -> Mix.Tasks.Register.run([]) end) end
    end

    test "invalid project", %{api_port: port} do
      assert capture_log(fn ->
               Mix.Tasks.Register.run([
                 "--host",
                 "127.0.0.1",
                 "--port",
                 "#{port}",
                 "--project",
                 "idonotexist",
                 "--email",
                 "test@test.test",
                 "--firstname",
                 "Test",
                 "--lastname",
                 "Test",
                 "--lrz",
                 "ab1337qx",
                 "--difficulty",
                 "1"
               ])
             end) =~ ~s(Register failed with code: 1234 and description: "invalid project")
    end

    test "valid", %{api_port: port} do
      assert capture_log(fn ->
               Mix.Tasks.Register.run([
                 "--host",
                 "127.0.0.1",
                 "--port",
                 "#{port}",
                 "--project",
                 "gossip",
                 "--email",
                 "test@test.test",
                 "--firstname",
                 "Test",
                 "--lastname",
                 "Test",
                 "--lrz",
                 "ab1337qx",
                 "--difficulty",
                 "1"
               ])
             end) =~ ~s(Successfully registered as team 1234! Please note that number!)
    end
  end
end
