# redefine the run module for testing
Code.compiler_options(ignore_module_conflict: true)

defmodule Mix.Tasks.Run do
  use ExUnit.Case

  def run(args) do
    assert args == ["--no-halt"]
    :ok
  end
end

defmodule Mix.Tasks.RunServer.Test do
  use ExUnit.Case, async: false
  import ExUnit.CaptureIO

  describe "run/1" do
    test "fails when config file is missing" do
      assert capture_io(:stderr, fn ->
               Mix.Tasks.RunServer.run([])
             end) =~ "Usage: mix run_server -c config.ini"
    end

    test "fails when config file does not exist" do
      assert_raise File.Error, fn ->
        Mix.Tasks.RunServer.run(["-c", "this_file_does_not_exist"])
      end
    end

    test "executes successfully" do
      Mix.Tasks.RunServer.run(["-c", "test/support/first.ini"])
    end
  end
end
