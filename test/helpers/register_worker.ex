defmodule VoidPhone.RegisterWorker do
  use GenServer, restart: :temporary

  @enroll_init 680
  @enroll_register 681
  @enroll_success 682
  @enroll_failure 683

  @impl true
  def init({socket, opts}) do
    difficulty = Keyword.get(opts, :pow_difficulty, 3)
    {:ok, packetizer} = VoidPhone.ApiPacketizer.start_link({socket, target: self()})

    {:ok, %{socket: socket, packetizer: packetizer, challenge: nil, difficulty: difficulty},
     {:continue, :send_init}}
  end

  @impl true
  def handle_continue(:send_init, state = %{socket: socket}) do
    challenge = :crypto.strong_rand_bytes(8)
    :gen_tcp.send(socket, VoidPhone.MessageUtils.build_message(@enroll_init, challenge))

    {:noreply, %{state | challenge: challenge}}
  end

  @impl true
  def handle_info({:tcp, _socket, data}, state = %{packetizer: packetizer}) do
    with :ok <- handle_message(data, state) do
      # tell the packetizer to receive the next message
      # used for back-pressure
      send(packetizer, :recv_new)
      {:noreply, state}
    end
  end

  @impl true
  def handle_info({:tcp_closed, _socket}, state) do
    {:stop, :normal, state}
  end

  # helper for the proof of work
  defp valid_pow?(difficulty, msg) do
    hash = :crypto.hash(:sha256, msg)

    case hash do
      <<0::size(difficulty)-unit(8), _rest::binary()>> ->
        true

      _other ->
        false
    end
  end

  # a handler for the api message
  defp handle_message(<<msgtype::integer-size(16), data::binary>>, %{
         socket: socket,
         challenge: challenge,
         difficulty: difficulty
       })
       when msgtype == @enroll_register do
    case data do
      <<^challenge::binary-size(8), _team_nr::integer-size(16), project_nr::integer-size(16),
        _nonce::binary-size(8), _encoded_params::binary()>> ->
        cond do
          project_nr not in [2961, 7071, 4963, 15882, 39943] ->
            :gen_tcp.send(
              socket,
              VoidPhone.MessageUtils.build_message(
                @enroll_failure,
                <<0::size(16), 1234::integer-size(16), "invalid project">>
              )
            )

          not valid_pow?(difficulty, data) ->
            :gen_tcp.send(
              socket,
              VoidPhone.MessageUtils.build_message(
                @enroll_failure,
                <<0::size(16), 666::integer-size(16), "invalid challenge">>
              )
            )

          true ->
            # answer with enroll success
            :gen_tcp.send(
              socket,
              VoidPhone.MessageUtils.build_message(
                @enroll_success,
                <<0::size(16), 1234::integer-size(16)>>
              )
            )
        end
    end
  end

  ## Client API ##

  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end
end
