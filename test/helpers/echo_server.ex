defmodule EchoServer do
  use GenServer

  @moduledoc """
  This server is a serial echo server.
  It accepts one client at a time and simply replies what it received.
  """

  @impl true
  def init(target) when is_pid(target) do
    {:ok, socket} = :gen_tcp.listen(0, [:binary, active: true])

    with {:ok, {_, port}} <- :inet.sockname(socket) do
      send(target, {self(), :port, port})
    end

    {:ok, socket, {:continue, :accept}}
  end

  @impl true
  def handle_continue(:accept, listening_socket) do
    {:ok, _socket} = :gen_tcp.accept(listening_socket)
    {:noreply, listening_socket}
  end

  @impl true
  def handle_info({:tcp, socket, data}, listening_socket) do
    :ok = :gen_tcp.send(socket, data)
    {:noreply, listening_socket}
  end

  ## Client API ##

  def start_link(opts), do: GenServer.start_link(__MODULE__, opts, [])
end
