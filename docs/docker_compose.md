# Setup using docker-compose

## Setting up multiple peers using docker-compose

To quickly test an environment with multiple nodes, the project includes a `docker-compose.yml` file in the `test/support` folder.

```bash
$ cd test/support
$ docker-compose up --build
...
node2_1  | 
node2_1  | 12:20:34.577 [info]  Starting Gossip...
...
```

You should be able to see the output from all three nodes. The log level is `info` by default. This can be changed in the `docker-compose.yml` file by modifying the `-l` parameter.

Now you can connect using the sample `gossip_client.py` script from the `voidphone_pytest` repository:

```bash
# in three new terminals
$ cd voidphone_pytest
# make sure the virtual env is active, then
$ python3 gossip_client.py -n -d 127.0.0.1 --port 8001
$ python3 gossip_client.py -n -d 127.0.0.1 --port 7001
$ python3 gossip_client.py -a -d 127.0.0.1 -p 9001
# this sends a message to node3 that will spread the information
# to the other two nodes
```

The first node will listen for API connections on port `7001`, the second one on port `8001` and the third one on port `9001`.

## Using the Demo Application

There also exists a demo web application that is automatically started when using the docker-compose sample.
The app listens on ports 7000, 8000 and 9000 and exposes an interface to send and receive messages, as well as connect to new peers.

Optionally (may not work in the future), you can have a look at the hosted demo sites:
* https://peer-a.voidphone.steffend.me
* https://peer-b.voidphone.steffend.me

You can connect on port 7001 for api connections and port 7002 for p2p connections, e.g. using the test client from the voidphone_pytest repo:

```bash
$ python3 gossip_client.py -a -d peer-a.voidphone.steffend.me -p 7001
```

A `deadbeef` message should arrive in those browsers that subscribed to type 1337.
