# Initial Report: Team Gossip 5

## 1. Basic Information

* Team Name: Gossip 5
* Team members: Steffen Deusch (ge49xoq)
* Module: Gossip

## 2. Language and OS

I've chosen to do the project in [Elixir](https://elixir-lang.org) as I really like the language for it's simplicity while still providing a powerful platform based on Erlang/OTP for building distributed and fault-tolerant applications. As Elixir is a language based on the BEAM virtual machine, the project can run on any operating system that supports the BEAM. Personally, I'm working on macOS, as this is the platform I've grown to work with.

## 3. Build System

Elixir comes with a built-in system for managing projects, called [Mix](https://hexdocs.pm/mix/Mix.html), that can be used to build releases or [escripts](https://hexdocs.pm/mix/1.12.0/Mix.Tasks.Escript.Build.html). For packing these releases, I'm planning to provide a simple Dockerfile.

## 4. Quality Enforcement

To ensure the quality of the project, I'm planning to use Elixir's great testing library [ExUnit](https://hexdocs.pm/ex_unit/ExUnit.html) that can be used to write mainly unit tests. For the API I'm also planning to test against the provided Python implementation.
As Elixir is a functional language, I'm trying to write plain functions whenever possible that can be easily tested. A great feature for ensuring quality of the documentation is the use of [Doctests](https://elixir-lang.org/getting-started/mix-otp/docs-tests-and-with.html#doctests) that provide real examples that are actually tested in the code documentation.

## 5. Libraries

Elixir has access to all the built-in libraries of Erlang/OTP, which by themselves provide a rich set of functionalities, e.g.:

* [gen_tcp](https://erlang.org/doc/man/gen_tcp.html) for working with TCP connections
* [crypto](https://erlang.org/doc/man/crypto.html) for working with anything crypto-related

And of course the great build-in Elixir libraries for building generic servers:

* [GenServer](https://hexdocs.pm/elixir/GenServer.html)

I'm not sure if I will use any external dependency, but maybe I'll use one for encoding binary-json, e.g.:

* [Cyanide](https://github.com/ispirata/cyanide)

Although the BEAM comes with great distribution mechanisms for working with multiple nodes, I won't use this in this project, as Erlang Distribution assumes that each node is fully trusted and allows all nodes to execute arbitrary code on every other node.
## 6. License

You can do anything with my code. If I have to choose a license, it would be the MIT license. Therefor I'm including a copy of it in this repository.

## 7. Experience

I've been programming for about 8 years, mainly using Python, but also C and Java. Lately I've been using a lot of JavaScript (Node.js) working as working student at a company. In the last year I discovered Elixir and learned to enjoy it a lot, writing a couple of private projects in it.

## 8. Workload Distribution

As I'm planning on doing this project on my own, all related work is done solely by me.