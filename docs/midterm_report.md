# Midterm Report: Team Gossip 5

## 1. Changes to my assumptions in the initial report

The only change to what is mentioned in the initial report is the use of the Erlang "External Term Format" for peer to peer messages instead of binary-json. More on that [below](#message-formats).

## 2. Architecture of my module

My module is implemented as an Erlang [OTP application](https://erlang.org/doc/man/application.html).
When started, the application starts the `VoidPhone.Gossip.Supervisor`. This module is a generic
`Supervisor` that itself just starts and monitors processes. The supervisor starts the following modules:

* `VoidPhone.Gossip.NotificationRegistry` - a `Registry` that is used to store the processes that are subscribed to a specific data type.
* `VoidPhone.Gossip.PeerRegistry` - a `Registry` for mapping peer addresses to the peer connection
* `VoidPhone.Gossip.Api` - the TCP entrypoint for the API server
* `VoidPhone.Gossip.ApiSupervisor` - a `DynamicSupervisor` that monitoring the process ids of connected API clients (*)
* `VoidPhone.Gossip.P2P` - the TCP entrypoint for the P2P connections
* `VoidPhone.Gossip.P2pSupervisor` - same as the ApiSupervisor, used for monitoring open P2P TCP connections (*)
* `VoidPhone.Gossip.PeerManager` - responsible for manaing peer connections, e.g. bootstrapping and connecting to learned peers

(*) Using a `DynamicSupervisor` is a best-practice in the Elixir world when dealing with started processes. One generally wants that all processes that one cares about are supervised by a supervisor. A generic `Supervisor` starts processes specified at compile-time, while a `DynamicSupervisor` is responsible for processes started at runtime.
Supervisors can restart their child processes if they crash, but we do not use this feature with our DynamicSupervisors as it is intentional that the processes handling the TCP connection exit when receiving malformed data or the TCP connection being closed.

### Processes and Networking

As Elixir runs on the BEAM VM that uses lightweight green-thread-like processes, every API and P2P connection gets their own process. While these processes
are not real operating system processes or even kernel threads, the BEAM VM does its best job to ensure that all available processor cores on the host are being used. BEAM processes are preemptive, therefore no single client can easily take over all the available resources.
As every connection gets its own process, the result is somewhat similar to asynchronous I/O in other languages in the sense that no single API client is able to block the execution of the system.

### Security

As Gossip is a low-level communication mechanism in the VoidPhone project and the actual client communication is encrypted by the Onion module, this Gossip implementation does not do any encryption.
The peer that is initiating the connection to another peer is required to complete a proof of work challenge with configurable difficulty to make sybil attacks harder. To prevent an evil target node to slow down the connecting peer with a very hard difficulty one can also configure a timeout for the proof of work challenge.

This module generally assumes that API clients (that normally only connect from the same host) are trustworthy. Still, when receiving a malformed API request (e.g. non-gossip message type), the connection is being terminated.

## 3. The peer-to-peer protocol

My implementation uses a TCP-based peer-to-peer protocol. The main reasons for using TCP is that
* TCP is a well-established standard
* when working with peers it makes sense to work connection-oriented
* I already implemented a generic TCP handler for the API

The peers are identified by their public p2p address.
### Message formats

Erlang offers a convenient way to convert data structures into a [binary format](https://erlang.org/doc/apps/erts/erl_ext_dist.html) (known as "External Term Format")
that offers a structured format for data exchange. Similar to binary-json, the format is not human readable. The tcp messages are structured in the following way:

* 2 bytes for the message length (limiting the maximum message size to 2^16 bytes) followed by the specified amount of bytes for the actual message (Erlang External Term Format)

Currently there are the following messages:

* `{:challenge_request, difficulty, data}` - this message is sent by the peer to which we're trying to connect and requires the connecting peer to do sha256-based proof of work with the given `difficulty` amount of leading zero bytes. `difficulty` is an integer, `data` arbitrary 16-byte binary data.

* `{:challenge_response, data, nonce}` - this message is sent by the connected peer once the proof of work is done. We can verify the data by calculating the sha256 hash of data <> nonce (<> means concatentation) and checking if it has the required amount of leading zeroes. `data` must be the same binary data as initially sent in the challenge request. `nonce` is arbitrary binary data (limited to 16 bytes in size).

All of following messages are only accepted by the receiving peer once the sending node completed the proof of work challenge and is therefore "verified".

* `{:p2p_address, address}` - this message informs the receiving peer of the address to which other peers are able to connect. This is necessary as the "server", i.e. the accepting peer does not necessarily know the port that the remote p2p server is listening on. `address` is a string in the same host:port format as used in the configuration files.

* `{:spread, message_id, ttl, data_type, data}` - this message tells other nodes that this node wants to spread the specified information. Other nodes that receive this message will decrease the ttl and forward the message to all local subscribers for verification. Once verified, the message will be spread further until the ttl is 1. The message id is based on a hash of a random number, the data type and data to prevent looping messages through the network. It is created by the node that initially receives the `GOSSIP_ANNOUNCEMENT`.

* `{:peer_announcement, peer_list}` where peer-list is a list of the known peers. Automatically sent to newly connected nodes and sent again once the list of known peers changes.

## 4. Future Work

Currently, the peers connect to any node sent in a `peer_announcement` message. The `max_connections` config parameter is ignored. This will be changed as I'm planning on implementing a Brahms-like push-pull principle for manaing the connected peers.

Also, I'm thinking about keeping track of the originating peer of a message in order to avoid looping messages. Currently, looped messages are received, but ignored, as their message id already exists in the knowledge base and therefore the notification is not sent again.

## 5. Workload Distribution

As specified in the initial report, I'm doing the project on my own. Therefore
all workload is done by me.

## 6. Effort spent for the project

I've spent many evenings mainly thinking about the p2p protocol and implementing it in parts. This is only partly visible in the Git commits as I still have to learn to commit more often and not only when things are "done". Still, looking at the commits gives a rough overview of what happened when.
Concerning actual coding time: I'm using [WakaTime](https://wakatime.com) to keep track of coding activities and from July 5th to July 11th it reports 12 hrs 45 mins spent on the void_phone project.
