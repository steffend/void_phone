# Using the IEx Shell

## Inspecting a Node

Assuming that Docker is used, an interactive session can be started using: `./run_interactive.sh -c config.ini -l warn`.

When using Elixir locally, the corresponding command would be `iex -S mix run_server -c config.ini -l warn`.

Note that we're setting the loglevel using `-l` to warn to prevent getting distracted:

```bash
$ ./run_interactive.sh -c config.ini -l warn
Erlang/OTP 24 [erts-12.0.3] [source] [64-bit] [smp:4:4] [ds:4:4:10] [async-threads:1]

Compiling 1 file (.ex)
Interactive Elixir (1.12.2) - press Ctrl+C to exit (type h() ENTER for help)
iex(1)> VoidPhone.Gossip.PeerManager.get_peers()
[]
```

Here we used the PeerManager to list the currently connected peers. Assuming that another peer runs with the api port 8001 and p2p port 8002, we can connect to this peer using: `VoidPhone.Gossip.PeerManager.connect("127.0.0.1:8002")`:

```elixir
iex(2)> VoidPhone.Gossip.PeerManager.connect("127.0.0.1:8002")
{:error, :econnrefused}
iex(3)> VoidPhone.Gossip.PeerManager.connect("host.docker.internal:8002")
:ok
iex(4)> VoidPhone.Gossip.PeerManager.get_peers()
[{{192, 168, 64, 2}, 8002}]
```

On this IEx shell, any public function as documented in the [Documentation](https://netintum.pages.gitlab.lrz.de/teaching/p2psec_projects_2021/Gossip-5/) can be invoked.
