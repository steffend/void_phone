# Final Report: Team Gossip 5

## 1. Project Architecture

As outlined in the midterm report, my module is implemented as an Erlang [OTP application](https://erlang.org/doc/man/application.html) using a process-based architecture. The module itself is written in the [Elixir](https://elixir-lang.org) language. Its main functionality can be broken down to the API part, the P2P part and the PeerManager that is responsible for connecting peers and managing the local view based on the Brahms Gossip algorithm [1].

When started, the application is bootstrapped by starting the `VoidPhone.Gossip.Supervisor`. This module is a generic `Supervisor` that itself just starts and monitors other processes. The supervisor starts the following modules:

* `VoidPhone.Gossip.Api` - the TCP entrypoint for the API server
* `VoidPhone.Gossip.NotificationRegistry` - a `Registry` that is used to store the api processes that are subscribed to a specific data type.
* `VoidPhone.Gossip.P2p` - the TCP entrypoint for the P2P connections
* `VoidPhone.Gossip.PeerManager` - responsible for manaing peer connections, e.g. bootstrapping and connecting to learned peers based on Brahms
* `VoidPhone.Gossip.PeerRegistry` - a `Registry` for mapping peer addresses to the peer connection. The `VoidPhone.Gossip.PeerManager` uses this registry for mapping network addresses to process ids.

There also exist worker modules for the API (`VoidPhone.Gossip.ApiWorker`) and P2P (`VoidPhone.Gossip.P2pWorker`) connections that implement the message handling. As is best practice in Elixir, the workers are supervised by `DynamicSupervisor`s.

Please notice: In this report I use the term "node" and "peer" interchangeably.

### Processes and Networking

As Elixir runs on the BEAM VM that uses lightweight green-thread-like processes, every API and P2P connection gets its own process. While these processes
are not real operating system processes or even kernel threads, the BEAM VM does its best job to ensure that all available processor cores on the host are being used, while also being preemptive. Therefore no single client can easily take over all the available resources.
As every connection gets its own process, the result is somewhat similar to asynchronous I/O in other languages in the sense that no single API client is able to block the execution of the system.

### Security

As Gossip is a low-level communication mechanism in the VoidPhone project and the actual client communication is encrypted by the Onion module, this Gossip implementation does not do any encryption nor employs any signatures.
The peer that is initiating the connection to another peer is required to complete a proof of work challenge with configurable difficulty to make sybil attacks harder. To prevent an evil target node from slowing down the connecting peer with a very hard difficulty one can also configure a timeout for the proof of work challenge.

This module generally assumes that API clients (that normally only connect from the same host) are trustworthy. Still, when receiving a malformed API request (e.g. non-gossip message type), the connection is terminated.

Peer-to-peer connections are not assumed to be trustworthy and are also terminated as soon as an unexpected message arrives.

### The Peer-to-Peer Protocol

My implementation uses a TCP-based peer-to-peer protocol where each peer is identified by its public ip-port combination.

For the messages themselves, the protocol uses the [Erlang External Term Format](https://erlang.org/doc/apps/erts/erl_ext_dist.html) that offers a structured format for data exchange. Similar to binary-json, the format is not human readable. The TCP messages are structured in the following way:

* 2 bytes for the message length (limiting the maximum message size to 2^16 bytes) followed by the specified amount of bytes for the actual message encoded in the Erlang External Term Format

Currently there are the following messages:

* `GOSSIP_P2P_CHALLENGE_REQUEST`
  * decoded as: `{:challenge_request, difficulty, data}`
  * description: This message is sent by the peer to which we're trying to connect and requires the connecting peer to do sha256-based proof of work with the given `difficulty` amount of leading zero bytes. `difficulty` is an integer, `data` arbitrary 16-byte binary data.

* `GOSSIP_P2P_CHALLENGE_RESPONSE`
  * decoded as: `{:challenge_response, data, nonce}`
  * description: This message is sent by the connected peer once the proof of work is done. We can verify the data by calculating the sha256 hash of data <> nonce (<> means concatentation) and checking if it has the required amount of leading zeroes. `data` must be the same binary data as initially sent in the challenge request. `nonce` is arbitrary binary data (limited to 16 bytes in size).

* `GOSSIP_P2P_ADDRESS`
  * decoded as: `{:p2p_address, address}`
  * description: This message is sent by the peer that initiated the p2p connection as the accepting node does not know the peer's public address (ip-port combination). The receiving node uses this address as the identifier for the node.

All of following messages are only accepted by the receiving peer once the sending node completed the proof of work challenge and is therefore "verified".

* `GOSSIP_P2P_SPREAD`
  * decoded as: `{:spread, message_id, ttl, data_type, data}` 
  * description: This message tells other nodes that this node wants to spread the specified information. Other nodes that receive this message will decrease the ttl and forward the message to all local subscribers for verification. Once verified, the message will be spread further until the ttl is 1. The message id is based on a hash of a random number, the data type and data to prevent looping messages through the network. It is created by the node that initially receives the `GOSSIP_ANNOUNCEMENT` API message and afterwards used as a global identifier for this message.

The following messages are used for peer management and are based on the Brahms Algorithm [1].

* `GOSSIP_P2P_PUSH_REQUEST`
  * decoded as: `:push_request`
  * description: The receiving peer uses this message to add the sender's id to the current push set.

* `GOSSIP_P2P_PULL_REQUEST`
  * decoded as: `:pull_request`
  * description: The receiving peer replies with a `GOSSIP_P2P_PULL_RESPONSE` message that includes the current local view, i.e. the currently connected peers.

* `GOSSIP_P2P_PULL_RESPONSE`
  * decoded as: `{:pull_response, peer_list}`
  * description: The receiving peer uses this message to update its current pull set.

* `GOSSIP_PING`
  * decoded as: `:ping`
  * description: Used to verify that a node is alive. The receiving node must reply with a `GOSSIP_PONG` message.

* `GOSSIP_PONG`
  * decoded as `:pong`
  * description: The reply to a `GOSSIP_PING` message. If an unexpected `GOSSIP_PONG` message arrives, the p2p connection will be terminated.

### Brahms Algorithm

As outlined in the section above, the peer-to-peer protocol uses an algorithm based on Brahms to decide which peers to connect to. Therefore the module **should** have the same properties that Brahms provides, namely that with high probability an attacker cannot entirely consume the local view of correct peers.

As the p2p implementation is connection-oriented (using TCP), the PeerManager only manages connections to the peers in the current local view. Peers that are removed from the local view (for example after a view update), will be disconnected, but they do remain in the sampler until a liveliness check fails or another peer replaces their position in the sampler list.

The parameters for the Brahms algorithm can be [configured](#configuration). The peer will always only try to connect to a specific amount of peers. If this limit is reached, all new connecting peers are treated equal to a *push request* from an already connected peer and therefore have the chance to be included in the local view after the next view update.

### Message flow

I want to explain the message flow in this Gossip implementation based on an example. Let's assume that there are two peers: A and B.

When a `GOSSIP_ANNOUNCEMENT` message is received by peer A,

  1. The receiving `VoidPhone.Gossip.ApiWorker` spreads the message locally to all api clients using the `VoidPhone.Gossip.Api.broadcast/6` function.
  2. The `VoidPhone.Gossip.Api` module sends `{:gossip_notification, global_id, data_type, data}` messages to all api clients that are registered in the `VoidPhone.Gossip.NotificationRegistry` for the specified `data_type`.
  3. The registered processes receive a `{:gossip_notification, global_id, data_type, data}` message and send a `GOSSIP_NOTIFICATION` message to the socket.
  4. The process that originally received the `GOSSIP_ANNOUNCEMENT` message calls the
     `VoidPhone.Gossip.P2p.spread/6` function to spread the message further.
  5. The `VoidPhone.Gossip.P2p.spread/6` function gets the current local view from the `VoidPhone.Gossip.PeerManager` and sends a `{:spread, message_id, ttl, data_type, data}` message to each connected peer process.
  6. The `VoidPhone.Gossip.P2pWorker` on Peer A receives the spread message and sends a `GOSSIP_P2P_SPREAD` message to Peer B.
   
All of the following steps happen on Peer B:

  7. The `VoidPhone.Gossip.P2pWorker` on Peer B receives the spread message and spreads the message locally to all api clients using the `VoidPhone.Gossip.Api.broadcast/6` function.
  8. The same as in steps 2 and 3 happens on Peer B.
  9. Upon receiving a `GOSSIP_VALIDATION` from any connected api client, the `VoidPhone.Gossip.P2p.handle_validation/4` function is executed which calls the `VoidPhone.Gossip.P2p.spread/6` function again to spread the message with decreased ttl to all nodes that are currently registered in the `VoidPhone.Gossip.PeerManager`.

This closes the circle. To actually prevent looping messages, the global message id is cached in the local knowledge base on each node and only spread to other peers if it is not yet known.

## 2. Software Documentation

### Configuration

The project expects a windows ini-like configuration file as described in the project specification.
The following configuration options are used by this implementation:

```ini
[gossip]
cache_size = 3
validation_cache_size = 256
max_connections = 30
bootstrapper = localhost:8002
api_address = localhost:7001
p2p_address = localhost:7002
p2p_challenge_difficulty = 2
p2p_challenge_timeout = 1000
p2p_challenge_recv_timeout = 10000
brahms_alpha = 0.45
brahms_beta = 0.45
brahms_gamma = 0.1
brahms_push_interval = 5
brahms_pull_interval = 5
brahms_update_interval = 10
brahms_sampler_check_interval = 60
```

* `cache_size` The maximum number of values in the local knowledge base. The knowledge base is used for temporarily storing messages while they are being validated by the api connections.
* `validation_cache_size` The maximum number of values in the validation cache. A message id is cached in there when a validation message arrives to prevent spreading it multiple times in the case that multiple api clients validate the message.
* `max_connections` The maximum number of peers the implementation will try to connect to.
* `bootstrapper` The `p2p_address` of a node that should be used as bootstrapper.
* `api_address` As described in the project specification. This will be parsed accordingly by the `VoidPhone.IpUtils.get_ip_and_port/1` function.
* `p2p_address` As described in the project specification.
* `p2p_challenge_difficulty` How many leading zero-bytes are required in the p2p proof-of-work challenge. A typical value is two or three. You can use 0 to disable the challenge.
* `p2p_challenge_timeout` To prevent malicious nodes from requiring an arbitrarily complex challenge, you can define a timeout for performing the challenge. This value is expected to be in milliseconds.
* `p2p_challenge_recv_timeout` To prevent waiting forever on challenge responses, you can define a timeout for clients completing the challenge. This value is expected to be in milliseconds.
* `brahms_alpha` The proportion of push values when performing a view update.
* `brahms_beta` The proportion of pull values when performing a view update.
* `brahms_gamma` The proportion of history samples when performing a view update; alpha, beta and gamma must sum to one.
* `brahms_push_interval` The number of seconds between two consecutive push requests.
* `brahms_pull_interval` The number of seconds between two consecutive pull requests.
* `brahms_update_interval` The number of seconds between two consecutive view updates (should be larger than the previous two values).
* `brahms_sampler_check_interval` The number of seconds between checking if sampled peers are still alive.

### Installing and Running the Software

There are two ways of running this module:

1. using Docker
2. using a locally installed version of Elixir

Both ways generally expect the path to the configuration file using `-c` (or `--config`).
Optionally, the log level can be changed using `-l` (or `--loglevel`) and accepts the loglevels as documented for the [Elixir Logger](https://hexdocs.pm/logger/1.12.3/Logger.html#module-levels).

When not having Elixir installed locally, the project can be run inside of a Docker container using the following commands, assuming the current working directory is the project and ports 7001 / 7002 should be used for api / p2p connections:

```bash
$ docker build -t void_phone .
$ docker run --rm -p 7001:7001 -p 7002:7002 -v /path/to/config.ini:/tmp/config.ini void_phone -c /tmp/config.ini -l info
```

or simply run the `run.sh` script, e.g. `./run.sh -c config.ini`.
To access an interactive Elixir shell, you can use `./run_interactive.sh -c config.ini`.

> Warning for non-Linux users:
Please make sure to map the API and P2P ports correctly. When running on Linux, you can use `--net=host` to use host networking for the container. This is **NOT** supported on macOS (and probably also not on Windows), so please make sure that the addresses specified in the config file use `host.docker.internal` instead of `localhost` or `127.0.0.1` when accessing the host machine in that case. Also make sure to modify the port mappings using `-p` in the commands or scripts used.

When Elixir (min version: 1.12) and Erlang (min version: 24) are installed locally:

1. Install the project dependencies using the command: `mix deps.get`
2. Run the module using either `mix run_server -c config.ini` or `iex -S mix run_server -c config.ini` with the latter option also giving an interactive Elixir shell.
 
There is also the option to generate an [escript](https://hexdocs.pm/mix/1.12/Mix.Tasks.Escript.Build.html), that generates an executable that only needs [Erlang](https://www.erlang.org) installed on the local machine, but using Docker is typically more practical.

### Running tests

Running the tests can be achieved either by using `mix test` or `mix test --cover` when Elixir is installed, or by using the `./test.sh` script in the project root (uses Docker).

### Known issues

* When configuring the Brahms algorithm to connect to a low amount of peers (for example three) and testing a network of four peers, it can happen that by chance three peers form a full mesh and the fourth peer is disconnected from the others. The disconnected peer will try to re-establish the connection to a bootstrapper if one is configured and therefore send a new push request, but if there is no bootstrapper it will be isolated. This should only happen when the `max_connections` parameter is low, as the chance of all peers dropping a specific one from their local view drops significantly the larger the amount of allowed connections.

* Peers cache global message ids in the knowledge base. When there is a lot of throughput it can happen that a message loops as the message id is already dropped from the cache when it arrives again. This can be mitigated by configuring the `cache_size` to an appropriate value, depending on the message throughput.

* The test coverage could be improved and although I tried to make all tests deterministic, sometimes it still happens that a test fails. Re-running the tests (using `mix test` or ./test.sh for running the tests in Docker) should work fine in that case.

## 3. Future Work

1. The implementation currently has an overhead when spreading messages, as there is no routing information where a message comes from. Therefore all nodes will always spread a message to all connected peers. Looping messages are prevented by not sending a message back to where it was received from and caching the seen message ids and only spreading when the id is new. This could be improved by sending a list of peer ids in the `GOSSIP_P2P_SPREAD` message that indicate the route that the message took in the network. A node could check this list and only spread the message to peers that are not in it yet. One could also make the length of the list configurable to prevent it from growing very large.
   
2. When no push / pull messages arrive in an update interval, the PeerManager does not update the local view. In the case that there are no connected peers and no bootstrapper is configured, but there are still known peers in the history sample, the PeerManager could use these known peers for bootstrapping again.

3. I'd like to explore using a connectionless approach for the P2P protocol. Using TCP I had to work around the problem that the accepting peer does not necessarily know the public ip-port combination of the connecting peer. Therefore the `GOSSIP_P2P_ADDRESS` message was introduced. I tried specifying the source port when creating a tcp socket to use the same port as the p2p server, but on Linux this lead to `eaddrinuse` errors, despite the four-tuple `src_addr,src_port,dst_addr,dst_port` would still be different. This actually worked fine on macOS, after a [bug](https://erlang.org/download/OTP-24.0.5.README) was fixed in Erlang. Also, with a connectionless approach one would not need to differentiate between connected peers and peers that only exist in the sampler history, as both could be handled the same and be periodically checked for liveness.

## 4. Workload Distribution

As specified in the initial and midterm report, I did the project on my own.
Therefore all work was done solely by myself.

## 5. Effort spent for the project

Since the midterm report, I spent the week from 30th of August to implement the Brahms Gossip algorithm and then focused mainly on testing and documenting the project. For the last two weeks, according to wakatime.com, I've spent about 37 hours in my editor working on the project. This does not include the time spent in my browser researching. A rough overview of what happened when can be obtained by looking at the Git commits. There also exists a (closed) merge request with all commits that happened since the midterm report.

## References

[1]: E. Bortnikov, M. Gurevich, I. Keidar, G. Kliot, and A. Shraer. Brahms: Byzantine resilient random member- ship sampling. Computer Networks, 53(13):2340–2359, 2009. (as seen here: http://www.cs.technion.ac.il/~gabik/publications/Brahms-COMNET.pdf)
