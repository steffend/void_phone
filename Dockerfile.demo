# see https://hexdocs.pm/phoenix/1.6.0-rc.0/releases.html#containers
ARG MIX_ENV="prod"

FROM hexpm/elixir:1.12.3-erlang-24.0.6-alpine-3.14.0 as build

# prepare build dir
WORKDIR /app

# install hex + rebar
RUN mix local.hex --force && \
    mix local.rebar --force

# set build ENV
ARG MIX_ENV
ENV MIX_ENV="${MIX_ENV}"

# install mix dependencies
COPY . ./

WORKDIR /app/gossip_demo
RUN mix deps.get
RUN mix deps.compile
RUN mix assets.deploy
RUN mix compile
RUN mix release

# Start a new build stage so that the final image will only contain
# the compiled release and other runtime necessities
FROM alpine:3.14.0 AS app
RUN apk add --no-cache libstdc++ openssl ncurses-libs

ARG MIX_ENV
ENV USER="elixir"

WORKDIR "/home/${USER}/app"
# Creates an unprivileged user to be used exclusively to run the Phoenix app
RUN \
  addgroup \
   -g 1000 \
   -S "${USER}" \
  && adduser \
   -s /bin/sh \
   -u 1000 \
   -G "${USER}" \
   -h /home/elixir \
   -D "${USER}" \
  && su "${USER}"

# Everything from this line onwards will run in the context of the unprivileged user.
USER "${USER}"

COPY --from=build --chown="${USER}":"${USER}" /app/gossip_demo/_build/"${MIX_ENV}"/rel/gossip_demo ./

ENTRYPOINT ["/home/elixir/app/bin/gossip_demo"]

# Usage:
#  * build: sudo docker image build -t elixir/gossip_demo .
#  * shell: sudo docker container run --rm -it --entrypoint "" -p 127.0.0.1:4000:4000 elixir/gossip_demo sh
#  * run:   sudo docker container run --rm -it -p 127.0.0.1:4000:4000 --name gossip_demo elixir/gossip_demo
#  * exec:  sudo docker container exec -it gossip_demo sh
#  * logs:  sudo docker container logs --follow --tail 100 gossip_demo
CMD ["start"]
