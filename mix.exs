defmodule VoidPhone.MixProject do
  use Mix.Project

  def project do
    [
      app: :void_phone,
      version: "0.1.0",
      elixir: "~> 1.12",
      elixirc_paths: compiler_paths(Mix.env()),
      elixirc_options: [warnings_as_errors: true],
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      escript: [main_module: VoidPhone.CLI, app: nil, embed_elixir: true],
      # docs
      name: "VoidPhone",
      source_url: "https://gitlab.lrz.de/netintum/teaching/p2psec_projects_2021/Gossip-5",
      docs: [
        # The main page in the docs
        main: "overview",
        extras: [
          "docs/initial_report.md",
          "docs/midterm_report.md",
          "docs/final_report.md",
          "docs/docker_compose.md",
          "docs/using_the_iex_shell.md",
          "README.md": [filename: "overview", title: "Overview"]
        ],
        groups_for_extras: [
          Reports: Path.wildcard("docs/*_report.md"),
          Guides: Path.wildcard("docs/*.md") -- Path.wildcard("docs/*_report.md")
        ],
        source_url_pattern:
          "https://gitlab.lrz.de/netintum/teaching/p2psec_projects_2021/Gossip-5/blob/master/%{path}#L%{line}"
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :crypto],
      mod: {VoidPhone.Application, []}
    ]
  end

  def compiler_paths(:test), do: ["test/helpers"] ++ compiler_paths(:prod)
  def compiler_paths(_), do: ["lib"]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
      {:ex_doc, "~> 0.24", only: :dev, runtime: false},
      {:liveness, "~> 1.0", only: :test},
      {:cachex, "~> 3.4"}
    ]
  end
end
