import Config

# Print only warnings and errors during test
config :logger, level: :critical

config :void_phone,
  gossip: %{
    "api_address" => "127.0.0.1:16001",
    "p2p_address" => "127.0.0.1:16002",
    "p2p_challenge_difficulty" => 1,
    "p2p_challenge_timeout" => 200,
    "p2p_challenge_recv_timeout" => 200,
    # set these to absurdly high values
    # to prevent the manager from actually sending those
    # without the tests triggering them
    "brahms_push_interval" => 600,
    "brahms_pull_interval" => 600,
    "brahms_update_interval" => 1200
  }
