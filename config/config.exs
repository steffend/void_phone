import Config

config :logger, :console,
  format: "$time [$level] $message $metadata\n",
  metadata: [:peer]

import_config "#{config_env()}.exs"
